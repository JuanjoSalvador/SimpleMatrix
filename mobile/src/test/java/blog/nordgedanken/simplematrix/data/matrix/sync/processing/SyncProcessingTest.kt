package blog.nordgedanken.simplematrix.data.matrix.sync.processing

import android.content.Context
import androidx.room.Room
import blog.nordgedanken.simplematrix.data.db.AppDatabase
import com.google.gson.JsonObject
import io.kamax.matrix.client.regular.SyncDataJson
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito.mock

class SyncProcessingTest {

  private lateinit var database: AppDatabase

  @Before
  fun initDb() {
    database = Room.inMemoryDatabaseBuilder(mock(Context::class.java),
        AppDatabase::class.java).build()
  }

  @After
  fun closeDb() {
    database.close()
  }

  @Test
  fun m_direct() {
    SyncProcessing(SyncDataJson(JsonObject()))
  }
}