package blog.nordgedanken.simplematrix.utils.db

import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Update

/**
 *
 * Simplifies the Dao's so that they don't require implementing always needed functions
 *
 * @author Marcel Radzio
 *
 */
interface BaseDao<T> {
  @Insert
  fun insertAll(obj: Iterable<T>)

  @Update
  fun updateAll(obj: Iterable<T>)

  @Delete
  fun delete(obj: T)
}