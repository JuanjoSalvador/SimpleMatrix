/*
 * SimpleMatrix - A simplified Android Matrix Client
 * Copyright (C) 2018 Marcel Radzio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package blog.nordgedanken.simplematrix.utils

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.ColorFilter
import android.graphics.PixelFormat
import android.graphics.drawable.Drawable
import android.util.TypedValue
import android.view.Gravity
import android.widget.LinearLayout
import android.widget.TextView
import java.util.*

/**
 * Created by MTRNord on 01.09.2018.
 */
class NameAvatar(context: Context, text: CharSequence) : Drawable() {
  @Transient
  private val mIntrinsicSize: Int
  @Transient
  private val mTextView: TextView

  init {
    mIntrinsicSize = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, DRAWABLE_SIZE.toFloat(),
        context.resources.displayMetrics).toInt()
    mTextView = createTextView(context, text)
    mTextView.width = mIntrinsicSize
    mTextView.height = mIntrinsicSize
    mTextView.measure(mIntrinsicSize, mIntrinsicSize)
    mTextView.layout(0, 0, mIntrinsicSize, mIntrinsicSize)
  }

  private fun createTextView(context: Context, text: CharSequence): TextView {
    val textView = TextView(context)
    //        textView.setId(View.generateViewId()); // API 17+
    val lp = LinearLayout.LayoutParams(
        LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
    lp.gravity = Gravity.CENTER
    textView.layoutParams = lp
    textView.gravity = Gravity.CENTER
    val rnd = Random()
    val randomColor = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256))
    textView.setBackgroundColor(randomColor)
    textView.setTextColor(Color.WHITE)
    textView.setTextSize(TypedValue.COMPLEX_UNIT_DIP, DEFAULT_TEXT_SIZE.toFloat())
    textView.text = text
    return textView
  }

  fun setText(text: CharSequence) {
    mTextView.text = text
    invalidateSelf()
  }

  override fun draw(canvas: Canvas) {
    mTextView.draw(canvas)
  }

  override fun getOpacity(): Int {
    return PixelFormat.OPAQUE
  }

  override fun getIntrinsicWidth(): Int {
    return mIntrinsicSize
  }

  override fun getIntrinsicHeight(): Int {
    return mIntrinsicSize
  }

  override fun setAlpha(alpha: Int) {}

  override fun setColorFilter(filter: ColorFilter?) {}

  companion object {
    private const val DRAWABLE_SIZE = 32 // device-independent pixels (DP)
    private const val DEFAULT_TEXT_SIZE = 12 // device-independent pixels (DP)
  }
}