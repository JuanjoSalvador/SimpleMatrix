package blog.nordgedanken.simplematrix.utils

import android.annotation.SuppressLint
import android.content.ContentResolver
import android.content.ContentUris
import android.content.Context
import android.database.Cursor
import android.net.Uri
import android.os.Build
import android.os.Environment
import android.provider.DocumentsContract
import android.provider.MediaStore
import android.provider.OpenableColumns
import android.webkit.MimeTypeMap
import java.io.IOException
import java.net.URISyntaxException


/**
 * Created by MTRNord on 06.02.2019.
 */
object MediaStoreHelper {
    @Throws(IOException::class)
    fun readBytesFromAndroidImageURL(context: Context, uri: Uri): ByteArray? =
            context.contentResolver.openInputStream(uri)?.buffered()?.use { it.readBytes() }

    fun getMimeType(context: Context, uri: Uri): String? {
        return if (ContentResolver.SCHEME_CONTENT == uri.scheme) {
            val cr = context.contentResolver
            cr.getType(uri)
        } else {
            val fileExtension = MimeTypeMap.getFileExtensionFromUrl(uri
                    .toString())
            MimeTypeMap.getSingleton().getMimeTypeFromExtension(
                    fileExtension.toLowerCase())
        }
    }

    fun getFileSize(context: Context, uri: Uri): Int? {
        if ("content".equals(uri.scheme!!, ignoreCase = true)) {
            val cursor: Cursor? = context.contentResolver
                    .query(uri, null, null, null, null)
            cursor.use {
                val columnIndex = it?.getColumnIndexOrThrow(OpenableColumns.SIZE)
                if (it?.moveToFirst()!!) {
                    return cursor?.getLong(columnIndex!!)?.toInt()
                }
            }

        }
        return null
    }

    @SuppressLint("NewApi")
    @Throws(URISyntaxException::class)
    fun getFilePath(context: Context, uri: Uri): String? {
        var uriL = uri
        var selection: String? = null
        var selectionArgs: Array<String>? = null
        // Uri is different in versions after KITKAT (Android 4.4), we need to
        if (Build.VERSION.SDK_INT >= 19 && DocumentsContract.isDocumentUri(context.applicationContext, uriL)) {
            when {
                isExternalStorageDocument(uriL) -> {
                    val docId = DocumentsContract.getDocumentId(uriL)
                    val split = docId.split(":".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                    return Environment.getExternalStorageDirectory().path + "/" + split[1]
                }
                isDownloadsDocument(uriL) -> {
                    val id = DocumentsContract.getDocumentId(uriL)
                    uriL = ContentUris.withAppendedId(
                            Uri.parse("content://downloads/public_downloads"), java.lang.Long.valueOf(id))
                }
                isMediaDocument(uriL) -> {
                    val docId = DocumentsContract.getDocumentId(uriL)
                    val split = docId.split(":".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                    val type = split[0]
                    when (type) {
                        "image" -> uriL = MediaStore.Images.Media.EXTERNAL_CONTENT_URI
                        "video" -> uriL = MediaStore.Video.Media.EXTERNAL_CONTENT_URI
                        "audio" -> uriL = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI
                    }
                    selection = "_id=?"
                    selectionArgs = arrayOf(split[1])
                }
            }
        }
        if ("content".equals(uriL.scheme!!, ignoreCase = true)) {
            if (isGooglePhotosUri(uriL)) {
                return uriL.lastPathSegment
            }

            val projection = arrayOf(MediaStore.Images.Media.DATA)
            val cursor: Cursor? = context.contentResolver
                    .query(uriL, projection, selection, selectionArgs, null)
            cursor.use {
                val columnIndex = it?.getColumnIndexOrThrow(MediaStore.Images.Media.DATA)
                if (it?.moveToFirst()!!) {
                    return it.getString(columnIndex!!)
                }
            }

        } else if ("file".equals(uriL.scheme!!, ignoreCase = true)) {
            return uriL.path
        }
        return null
    }

    private fun isExternalStorageDocument(uri: Uri): Boolean {
        return "com.android.externalstorage.documents" == uri.authority
    }

    private fun isDownloadsDocument(uri: Uri): Boolean {
        return "com.android.providers.downloads.documents" == uri.authority
    }

    private fun isMediaDocument(uri: Uri): Boolean {
        return "com.android.providers.media.documents" == uri.authority
    }

    private fun isGooglePhotosUri(uri: Uri): Boolean {
        return "com.google.android.apps.photos.content" == uri.authority
    }
}