package blog.nordgedanken.simplematrix.utils.deeplinks

import com.airbnb.deeplinkdispatch.DeepLinkModule

/** This will generate a AppDeepLinkModuleLoader class */
@DeepLinkModule
class AppDeepLinkModule {

}