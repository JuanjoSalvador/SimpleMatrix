/*
 * SimpleMatrix - A simplified Android Matrix Client
 * Copyright (C) 2018 Marcel Radzio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package blog.nordgedanken.simplematrix

import android.os.Bundle
import android.view.View
import android.widget.ExpandableListAdapter
import android.widget.ExpandableListView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import blog.nordgedanken.simplematrix.data.view.User
import blog.nordgedanken.simplematrix.utils.HeaderInfo
import blog.nordgedanken.simplematrix.utils.UserDataListAdapter
import blog.nordgedanken.simplematrix.utils.db.DBHelpers
import com.google.android.material.appbar.CollapsingToolbarLayout
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_user_profile.*
import org.jetbrains.anko.doAsync

/**
 *
 * TODO DOCUMENT WHEN IMPLEMENTED PROPERLY
 *
 * @author Marcel Radzio
 *
 */
class UserProfile : AppCompatActivity() {
    private val groupsList = ArrayList<HeaderInfo>()

    private var listAdapter: UserDataListAdapter? = null
    private var expandableListView: ExpandableListView? = null

    private var user: User? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user_profile)
        setSupportActionBar(toolbar)
        share_fab.setOnClickListener { view ->
            Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show()
        }
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        val mxid = intent.getStringExtra("MXID")
        val roomID = intent.getStringExtra("roomID")
        doAsync {
            user = DBHelpers.getUserByMXIDRoomIDFromCache(mxid, roomID)

            runOnUiThread {
                setupStrings()
                setupList()
            }
        }
    }

    private fun setupList() {
        //get reference to the ExpandableListView
        expandableListView = (findViewById<View>(R.id.userdata_list) as ExpandableListView)

        initGroups()

        //create the adapter by passing your ArrayList data
        listAdapter = UserDataListAdapter(this, groupsList)
        //attach the adapter to the list
        expandableListView?.setAdapter(listAdapter)

        setListViewHeight(expandableListView!!, 0)
        setListViewHeight(expandableListView!!, 1)
        setListViewHeight(expandableListView!!, 2)
    }

    private fun initGroups() {
        val headerInfoUserOptions = HeaderInfo()
        headerInfoUserOptions.name = "User-Options"
        headerInfoUserOptions.user = user
        groupsList.add(headerInfoUserOptions)

        val headerInfoDirectChats = HeaderInfo()
        headerInfoDirectChats.name = "Direct-Chats"
        headerInfoDirectChats.user = user
        groupsList.add(headerInfoDirectChats)

        val headerInfoDevices = HeaderInfo()
        headerInfoDevices.name = "Devices"
        headerInfoDevices.user = user
        groupsList.add(headerInfoDevices)
    }

    private fun setupStrings() {
        val toolbartitle = findViewById<CollapsingToolbarLayout>(R.id.toolbar_layout)
        val title = findViewById<TextView>(R.id.title)
        val subTitle = findViewById<TextView>(R.id.subtitle)
        val status = findViewById<TextView>(R.id.status)

        toolbartitle.title = user?.name
        title.text = user?.name
        subTitle.text = user?.MXID
        status.text = if (user?.isOnline!!) {
            "online"
        } else {
            "offline"
        }
    }

    private fun setListViewHeight(listView: ExpandableListView,
                                  group: Int) {
        val listAdapter = listView.expandableListAdapter as ExpandableListAdapter
        var totalHeight = 0
        val desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.width,
                View.MeasureSpec.EXACTLY)
        for (i in 0 until listAdapter.groupCount) {
            val groupItem = listAdapter.getGroupView(i, false, null, listView)
            groupItem.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED)

            totalHeight += groupItem.measuredHeight

            if (listView.isGroupExpanded(i) && i != group || !listView.isGroupExpanded(i) && i == group) {
                for (j in 0 until listAdapter.getChildrenCount(i)) {
                    val listItem = listAdapter.getChildView(i, j, false, null,
                            listView)
                    listItem.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED)

                    totalHeight += listItem.measuredHeight

                }
            }
        }

        val params = listView.layoutParams
        var height = totalHeight + listView.dividerHeight * (listAdapter.groupCount - 1)
        if (height < 10)
            height = 200
        params.height = height
        listView.layoutParams = params
        listView.requestLayout()

    }
}
