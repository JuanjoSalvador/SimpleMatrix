package blog.nordgedanken.simplematrix.di

import android.accounts.Account
import android.accounts.AccountManager
import android.app.NotificationManager
import android.content.Context
import blog.nordgedanken.simplematrix.State
import blog.nordgedanken.simplematrix.data.db.AppDatabase
import blog.nordgedanken.simplematrix.roomView.tabFragments.ChatsFragment
import blog.nordgedanken.simplematrix.roomView.tabFragments.DirectChatsFragment
import blog.nordgedanken.simplematrix.roomView.tabFragments.FavsFragment
import org.koin.dsl.module.module

/**
 * Created by MTRNord on 04.02.2019.
 */
class AppModule(private val context: Context) {
    val definition = module {

        // Fragments
        single {
            ChatsFragment()
        }

        single {
            FavsFragment()
        }

        single {
            DirectChatsFragment()
        }

        single {
            AccountManager.get(context)
        }

        factory<Array<Account>> {
            get<AccountManager>().getAccountsByType("org.matrix")
        }

        single {
            State
        }

        single {
            AppDatabase.getInstance(context)
        }

        single {
            context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        }
    }
}