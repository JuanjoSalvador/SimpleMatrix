/*
 * SimpleMatrix - A simplified Android Matrix Client
 * Copyright (C) 2018 Marcel Radzio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package blog.nordgedanken.simplematrix

import android.Manifest
import android.accounts.Account
import android.accounts.AccountManager
import android.accounts.AccountManagerCallback
import android.accounts.AccountManagerFuture
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import blog.nordgedanken.matrix_android_sdk.Matrix
import blog.nordgedanken.simplematrix.accounts.MatrixAccountAuthenticator
import blog.nordgedanken.simplematrix.data.matrix.MatrixClient
import blog.nordgedanken.simplematrix.roomView.RoomsActivity
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.GoogleApiAvailability
import com.google.firebase.messaging.FirebaseMessaging
import com.orhanobut.logger.Logger
import io.kamax.matrix.MatrixID
import io.sentry.Sentry
import io.sentry.event.BreadcrumbBuilder
import org.jetbrains.anko.doAsync
import org.koin.android.ext.android.inject

/**
 * Main Entry Activity.
 * This Activity currently is just a placeholder which decides to either lauch the Login Activity
 * or the Rooms Activity.
 *
 * It is supposed to later on handle Multi Accounts!
 *
 * @author Marcel Radzio
 *
 */
class MainActivity : AppCompatActivity() {
    /**
     * Account manager is needed to work with Android Accounts
     */
    val am: AccountManager by inject()

    /**
     * Holds any available Accounts
     */
    val accounts: Array<Account> by inject()

    companion object {
        /**
         * Id to identify a internet permission request.
         */
        const val REQUEST_INTERNET = 0

        /**
         * If true the User does a fresh Login.
         * If false the user does a regular recreation of the Client Object using the saved token.
         */
        var firstLogin = false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        permissions()

        checkAccounts()
    }

    /**
     * Checks the [blog.nordgedanken.simplematrix.MainActivity.accounts] Array for existing Accounts
     * of the type org.matrix.
     *
     * If No accounts are present in the Array aka. it is Empty it launches a new Login Request.
     * Else it uses the first account in the Array to login with.
     *
     * TODO Make this Multi Account Capable
     */
    private fun checkAccounts() {
        if (accounts.isEmpty()) {
            firstLogin = true
            Sentry.getContext().recordBreadcrumb(
                    BreadcrumbBuilder().setMessage("First login").build()
            )
            am.addAccount("org.matrix", "token", null, Bundle(), this, OnTokenAcquired(), null)
        } else {
            // TODO rework to show available Accounts to the User and let him choose
            am.getAuthToken(accounts[0], MatrixAccountAuthenticator.TOKEN_TYPE, null, false, OnTokenAcquired(), null)
        }
    }

    private inner class OnTokenAcquired : AccountManagerCallback<Bundle> {

        override fun run(result: AccountManagerFuture<Bundle>?) {
            Logger.d("login")
            findViewById<blog.nordgedanken.simplematrix.utils.ui.ProgressBar>(R.id.load_progress).visibility = View.VISIBLE
            if (result !== null) {
                val bundle: Bundle? = try {
                    result.result
                } catch (e: Exception) {
                    Sentry.capture(e)
                    null
                }

                val launch = bundle?.get(AccountManager.KEY_INTENT) as? Intent
                if (launch != null) {
                    startActivityForResult(launch, 0)
                } else {
                    doAsync {
                        if (!firstLogin) {
                            val token = bundle
                                    ?.getString(AccountManager.KEY_AUTHTOKEN)

                            MatrixClient.token = token
                            val id = MatrixID.Builder(am.getUserData(accounts[0], "idString")).acceptable()

                            MatrixClient().relogin(am.getUserData(accounts[0], "baseUrl"), am.getUserData(accounts[0], "domain")!!, id)

                            // TODO properly do this when actually implementing the new SDK
                            Matrix(id, am.getUserData(accounts[0], "baseUrl"), am.getUserData(accounts[0], "domain")!!, token!!)
                        }

                        if (GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(this@MainActivity) == ConnectionResult.SUCCESS) {
                            FirebaseMessaging.getInstance().isAutoInitEnabled = true
                        }
                        val roomsIntent = Intent(this@MainActivity, RoomsActivity::class.java)
                        startActivity(roomsIntent)
                        this@MainActivity.finish()
                    }
                }
            } else {
                throw Exception("Login failed because AccountManagerFuture is null")
            }
            Logger.d("login done")
        }
    }

    private fun permissions() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.INTERNET)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission is not granted
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                            Manifest.permission.INTERNET)) {
                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this,
                        arrayOf(Manifest.permission.INTERNET),
                        REQUEST_INTERNET)

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }

        } else {
            Logger.d("granted")
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int,
                                            permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            REQUEST_INTERNET -> {
                // If request is cancelled, the result arrays are empty.
                if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    Logger.d("granted")
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                } else {
                    Logger.d("denied")
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return
            }

            // Add other 'when' lines to check for other
            // permissions this app might request.
            else -> {
                // Ignore all other requests.
            }
        }
    }
}
