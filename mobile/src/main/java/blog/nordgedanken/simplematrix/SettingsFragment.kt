package blog.nordgedanken.simplematrix

import android.os.Bundle
import androidx.preference.PreferenceFragmentCompat

/**
 * Created by MTRNord on 03.01.2019.
 */
class SettingsFragment : PreferenceFragmentCompat() {
    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        setPreferencesFromResource(R.xml.preferences, rootKey)
    }

}