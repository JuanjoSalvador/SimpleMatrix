package blog.nordgedanken.simplematrix

/**
 * Created by MTRNord on 04.02.2019.
 */
object State {
    var loadingDone: Boolean = false
    var loadingBacklogOf: MutableList<String> = mutableListOf()
}