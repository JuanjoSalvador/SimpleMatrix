/**
 * This is used to generate Databinding Classes used for Epoxy elements
 *
 * @author Marcel Radzio
 */

@EpoxyDataBindingLayouts({
        R.layout.room_layout_element,
        R.layout.incoming_text_message_layout_element,
        R.layout.incoming_image_message_layout_element,
        R.layout.outgoing_text_message_layout_element,
        R.layout.outgoing_image_message_layout_element
})
package blog.nordgedanken.simplematrix;

import com.airbnb.epoxy.EpoxyDataBindingLayouts;