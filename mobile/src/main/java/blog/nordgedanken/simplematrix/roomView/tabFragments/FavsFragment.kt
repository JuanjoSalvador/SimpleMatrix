/*
 * SimpleMatrix - A simplified Android Matrix Client
 * Copyright (C) 2018 Marcel Radzio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package blog.nordgedanken.simplematrix.roomView.tabFragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.paging.PagedList
import blog.nordgedanken.simplematrix.R
import blog.nordgedanken.simplematrix.data.matrix.sync.SyncStarter.initialSyncDone
import blog.nordgedanken.simplematrix.data.view.RoomFull
import com.orhanobut.logger.Logger

/**
 * Created by MTRNord on 21.07.2018.
 * A simple [Fragment] subclass.
 */
class FavsFragment : Tab() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val roomObserver = Observer<PagedList<RoomFull>> { rooms ->
            pagingController.submitList(rooms)
        }
        initialSyncDone.observe(this, Observer {
            if (it) {
                viewModel.favRoomList.observe(this, roomObserver)
            } else {
                Logger.d("bug")
                viewModel.favRoomList.removeObserver(roomObserver)
            }
        })
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                     savedInstanceState: Bundle?): View {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_favs_chats, container, false)
        initAdapter(view)
        return view
    }
}