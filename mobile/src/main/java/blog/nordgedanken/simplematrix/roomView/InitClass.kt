package blog.nordgedanken.simplematrix.roomView

import android.app.ActivityOptions
import android.content.Intent
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.graphics.drawable.DrawableCompat
import androidx.viewpager.widget.ViewPager
import blog.nordgedanken.simplematrix.R
import blog.nordgedanken.simplematrix.SettingsActivity
import blog.nordgedanken.simplematrix.State
import blog.nordgedanken.simplematrix.data.matrix.MatrixClient
import blog.nordgedanken.simplematrix.roomView.tabFragments.ChatsFragment
import blog.nordgedanken.simplematrix.roomView.tabFragments.DirectChatsFragment
import blog.nordgedanken.simplematrix.roomView.tabFragments.FavsFragment
import blog.nordgedanken.simplematrix.utils.ViewPagerAdapter
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.tabs.TabLayout
import org.jetbrains.anko.doAsync
import org.koin.android.ext.android.inject

abstract class InitClass : AppCompatActivity() {
    private val adapter: ViewPagerAdapter by lazy { ViewPagerAdapter(supportFragmentManager) }

    private val state by inject<State>()

    // Fragments
    private val chatsFragment by inject<ChatsFragment>()
    private val favsFragment by inject<FavsFragment>()
    private val directChatsFragment by inject<DirectChatsFragment>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (MatrixClient.client !== null) {
            if (!state.loadingDone) {
                doAsync {
                    MatrixClient().loadCache(this@InitClass)
                    state.loadingDone = true
                }
            }
        } else {
            findViewById<TextView>(R.id.sync_status)?.text = getString(R.string.error, "Login failed")
            findViewById<TextView>(R.id.sync_status)?.setTextColor(Color.RED)
        }
    }

    protected fun setupTabs(tabLayout: TabLayout, viewPager: ViewPager) {
        //Initializing viewPager
        setupViewPager(viewPager)

        //Initializing the tablayout
        tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                viewPager.setCurrentItem(tab.position, false)
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    tab.icon?.setTint(ContextCompat.getColor(this@InitClass, R.color.secondaryColor))
                } else {
                    val normalDrawable = tab.icon
                    val wrapDrawable = DrawableCompat.wrap(normalDrawable!!)
                    DrawableCompat.setTint(wrapDrawable, ContextCompat.getColor(this@InitClass, R.color.secondaryColor))
                    tab.icon = wrapDrawable
                }
            }

            override fun onTabUnselected(tab: TabLayout.Tab) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    tab.icon?.setTint(ContextCompat.getColor(this@InitClass, android.R.color.black))
                } else {
                    val normalDrawable = tab.icon
                    val wrapDrawable = DrawableCompat.wrap(normalDrawable!!)
                    DrawableCompat.setTint(wrapDrawable, ContextCompat.getColor(this@InitClass, android.R.color.black))
                    tab.icon = wrapDrawable
                }
            }

            override fun onTabReselected(tab: TabLayout.Tab) {}
        })

        viewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {}

            override fun onPageSelected(position: Int) {
                tabLayout.getTabAt(position)?.select()
            }

            override fun onPageScrollStateChanged(state: Int) {}
        })


        val tab = tabLayout.getTabAt(1)
        tab?.select()
    }

    protected fun setupFab(viewPager: ViewPager) {
        val fab = findViewById<FloatingActionButton>(R.id.create_chat)
        fab.setOnClickListener { view ->
            val fragment = supportFragmentManager.findFragmentByTag("android:switcher:" + R.id.viewpager + ":" + viewPager.currentItem)
            // Todo make it actually do real stuff
            when (fragment) {
                is FavsFragment -> {
                    fragment.addDialog()
                }
                is DirectChatsFragment -> {
                    fragment.addDialog()
                }
                is ChatsFragment -> {
                    fragment.addDialog()
                }
                else -> Snackbar.make(view, "Unable to add Dialog", Snackbar.LENGTH_LONG).show()
            }
        }
    }

    private fun setupViewPager(viewPager: ViewPager) {
        adapter.addFragment(2, directChatsFragment, resources.getString(R.string.tab_direct_chats))
        adapter.addFragment(1, chatsFragment, resources.getString(R.string.tab_chats))
        adapter.addFragment(0, favsFragment, resources.getString(R.string.tab_favorites))
        viewPager.adapter = adapter
        viewPager.offscreenPageLimit = 4
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        R.id.action_settings -> {
            val intent = Intent(this, SettingsActivity::class.java)
            // Check if we're running on Android 5.0 or higher
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                // Apply activity transition
                startActivity(intent,
                        ActivityOptions.makeSceneTransitionAnimation(this).toBundle())
            } else {
                // Swap without transition
                startActivity(intent,
                        null)
            }
            true
        }

        else -> {
            // If we got here, the user's action was not recognized.
            // Invoke the superclass to handle it.
            super.onOptionsItemSelected(item)
        }
    }
}