package blog.nordgedanken.simplematrix.roomView.recyclerView

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.text.PrecomputedTextCompat
import androidx.core.widget.TextViewCompat
import androidx.databinding.BindingAdapter
import blog.nordgedanken.simplematrix.GlideApp
import blog.nordgedanken.simplematrix.data.view.RoomFull
import blog.nordgedanken.simplematrix.data.view.User
import blog.nordgedanken.simplematrix.utils.ImageLoader


@BindingAdapter("imageUrl", "room")
fun ImageView.setImageUrl(url: String?, room: RoomFull?) {
    val glide = GlideApp.with(this.rootView)
    val imageLoader = ImageLoader(this.context, glide)
    if (url !== null && room !== null) {
        imageLoader.loadImage(this, url, room)
    }
}

@BindingAdapter("imageUrl", "user")
fun ImageView.setImageUrl(url: String?, user: User?) {
    val glide = GlideApp.with(this.rootView)
    val imageLoader = ImageLoader(this.context, glide)
    if (url !== null && user !== null) {
        imageLoader.loadImage(this, url, user)
    }
}

@BindingAdapter("messageImgUrl")
fun ImageView.setImageUrl(url: String?) {
    val glide = GlideApp.with(this.rootView)
    val imageLoader = ImageLoader(this.context, glide)
    if (url !== null) {
        imageLoader.loadImage(this, url)
    }
}

@BindingAdapter(
        "asyncText",
        "android:textSize",
        requireAll = false)
fun asyncText(view: TextView, text: CharSequence?, textSize: Int?) {
    if (text != null) {
        // first, set all measurement affecting properties of the text
        // (size, locale, typeface, direction, etc)
        if (textSize != null) {
            // interpret the text size as SP
            view.textSize = textSize.toFloat()
        }
        val params = TextViewCompat.getTextMetricsParams(view)
        (view as AppCompatTextView).setTextFuture(
                PrecomputedTextCompat.getTextFuture(text, params, null))
    }
}

@BindingAdapter("bind:layout_height")
fun setLayoutHeight(view: View, height: Int) {
    if (height != 0) {
        val layoutParams = view.layoutParams
        layoutParams.height = height
        view.layoutParams = layoutParams
    }
}

@BindingAdapter("bind:layout_width")
fun setLayoutWidth(view: View, width: Int) {
    if (width != 0) {
        val layoutParams = view.layoutParams
        layoutParams.width = width
        view.layoutParams = layoutParams
    }
}