package blog.nordgedanken.simplematrix.roomView.recyclerView

import android.content.Context
import android.content.Intent
import androidx.core.content.ContextCompat.startActivity
import blog.nordgedanken.simplematrix.RoomLayoutElementBindingModel_
import blog.nordgedanken.simplematrix.chatView.ChatRoom
import blog.nordgedanken.simplematrix.data.view.RoomFull
import com.airbnb.epoxy.EpoxyModel
import com.airbnb.epoxy.paging.PagedListEpoxyController
import io.sentry.Sentry
import io.sentry.event.BreadcrumbBuilder


class PagingRoomController(val context: Context) : PagedListEpoxyController<RoomFull>() {
  override fun buildItemModel(currentPosition: Int, item: RoomFull?): EpoxyModel<*> {
    return if (item == null) {
      RoomLayoutElementBindingModel_()
              .id(-currentPosition)
    } else {
      RoomLayoutElementBindingModel_()
              .id(item.room?.id)
              .room(item)
              .onClick { _ ->
                Sentry.getContext().recordBreadcrumb(
                        BreadcrumbBuilder().setMessage("User is going to open a ChatRoom").build()
                )
                val intent = Intent(context, ChatRoom::class.java).putExtra("roomID", item?.room?.id)
                  // Swap without transition
                  startActivity(context, intent,
                          null)
              }
    }
  }

  init {
    isDebugLoggingEnabled = true
  }

  override fun onExceptionSwallowed(exception: RuntimeException) {
    throw exception
  }
}