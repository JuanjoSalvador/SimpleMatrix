package blog.nordgedanken.simplematrix.roomView

import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.viewpager.widget.ViewPager
import blog.nordgedanken.simplematrix.R
import com.google.android.material.tabs.TabLayout

class RoomsActivity : InitClass() {

    //This is our tablayout
    val tabLayout: TabLayout by lazy { findViewById<TabLayout>(R.id.tablayout) }

    //This is our viewPager
    val viewPager: ViewPager by lazy { findViewById<ViewPager>(R.id.viewpager) }


    companion object {
        val syncStatus: MutableLiveData<String> by lazy {
            MutableLiveData<String>()
        }
        val syncStatusVisibility: MutableLiveData<Int> by lazy {
            MutableLiveData<Int>()
        }
        val loadProgressVisibility: MutableLiveData<Int> by lazy {
            MutableLiveData<Int>()
        }

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_rooms)
        setSupportActionBar(findViewById(R.id.toolbar))

        if (syncStatus.value != getString(R.string.starting_initial_sync) && syncStatus.value != getString(R.string.starting_initial_sync_with_retries) && syncStatus.value != getString(R.string.processing_initial_sync_response) && syncStatus.value != null) {
            loadProgressVisibility.value = View.VISIBLE
            syncStatusVisibility.value = View.VISIBLE
            syncStatus.value = getString(R.string.starting)
        }

        // Create the observer which updates the UI.
        val syncStatusObserver = Observer<String> { syncStatus ->
            // Update the UI, in this case, a TextView.
            findViewById<TextView>(R.id.sync_status)?.text = syncStatus
        }
        val syncStatusVisibilityObserver = Observer<Int> { syncStatusVisibility ->
            // Update the UI, in this case, a TextView.
            findViewById<TextView>(R.id.sync_status)?.visibility = syncStatusVisibility
        }
        val loadProgressVisibilityObserver = Observer<Int> { loadProgressVisibility ->
            // Update the UI, in this case, a TextView.
            findViewById<blog.nordgedanken.simplematrix.utils.ui.ProgressBar>(R.id.load_progress)?.visibility = loadProgressVisibility
        }

        syncStatus.observe(this, syncStatusObserver)
        syncStatusVisibility.observe(this, syncStatusVisibilityObserver)
        loadProgressVisibility.observe(this, loadProgressVisibilityObserver)

        setupTabs(tabLayout, viewPager)
        setupFab(viewPager)
    }
}
