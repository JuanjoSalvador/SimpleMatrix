package blog.nordgedanken.simplematrix.roomView.tabFragments

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import blog.nordgedanken.simplematrix.R
import blog.nordgedanken.simplematrix.data.view.RoomViewModel
import blog.nordgedanken.simplematrix.roomView.recyclerView.PagingRoomController
import com.airbnb.epoxy.EpoxyRecyclerView

/**
 * Created by MTRNord on 11.10.2018.
 */
abstract class Tab : Fragment() {
    var recyclerView: EpoxyRecyclerView? = null
    protected val pagingController: PagingRoomController by lazy { PagingRoomController(context!!) }

    protected val viewModel by lazy(LazyThreadSafetyMode.NONE) {
        ViewModelProviders.of(this).get(RoomViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    protected fun initAdapter(view: View) {
        recyclerView = view.findViewById(R.id.recycler_view)

        recyclerView?.layoutManager = LinearLayoutManager(context!!)
        recyclerView?.setController(pagingController)
    }

    // Todo make it actually do real stuff
    fun addDialog() {
        /*val calendar = Calendar.getInstance()
        val i = dialogsListAdapter!!.itemCount
        calendar.add(Calendar.DAY_OF_MONTH, -(i * i))
        calendar.add(Calendar.MINUTE, -(i * i))
        dialogsListAdapter!!.addItem(0, DialogsFixtures.getDialog(i, Date()))
        dialogsListView?.smoothScrollToPosition(0)*/
    }
}