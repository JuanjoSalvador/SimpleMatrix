/*
 * SimpleMatrix - A simplified Android Matrix Client
 * Copyright (C) 2018 Marcel Radzio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package blog.nordgedanken.simplematrix

import android.annotation.SuppressLint
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import androidx.core.app.NotificationManagerCompat
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.multidex.MultiDexApplication
import blog.nordgedanken.matrix_android_sdk.di.SDKAppModule
import blog.nordgedanken.simplematrix.di.AppModule
import blog.nordgedanken.simplematrix.utils.Notification
import com.airbnb.deeplinkdispatch.DeepLinkHandler
import com.facebook.stetho.Stetho
import com.orhanobut.logger.AndroidLogAdapter
import com.orhanobut.logger.Logger
import io.sentry.Sentry
import io.sentry.android.AndroidSentryClientFactory
import org.koin.log.EmptyLogger
import org.koin.standalone.StandAloneContext.startKoin

/**
 *
 * Initialises the Logger, Sentry, Stetho and the needed Notification channels
 *
 * @author Marcel Radzio
 */
class App : MultiDexApplication(), LifecycleObserver {

    companion object {
        var appState: String = "background"

        private const val PREFS_NAME = "Prefs"
        private const val PREF_VERSION_CODE_KEY = "version_code"
        private const val DOESNT_EXIST = -1
    }

    override fun onCreate() {
        super.onCreate()
        checkFirstRun()
        Logger.addLogAdapter(AndroidLogAdapter())
        Sentry.init("https://b4ceee1647a94237bff8cb245fc9231a@sentry.io/1243764", AndroidSentryClientFactory(this))
        startKoin(listOf(AppModule(this).definition, SDKAppModule(this).definition), logger = EmptyLogger())
        Stetho.initializeWithDefaults(this)
        Notification.createNotificationChannel(this, Notification.ChatsChannelID)
        Notification.createNotificationChannel(this, Notification.DirectChatsChannelID)
        Notification.createNotificationChannel(this, Notification.FavChatsChannelID)
        Notification.createNotificationChannel(this, Notification.LISTENING_FOR_EVENTS_NOTIFICATION)

        val intentFilter = IntentFilter(DeepLinkHandler.ACTION)
        LocalBroadcastManager.getInstance(this).registerReceiver(DeepLinkReceiver(), intentFilter)
    }

    @SuppressLint("ApplySharedPref")
    private fun checkFirstRun() {
        // Get current version code
        val currentVersionCode = BuildConfig.VERSION_CODE

        // Get saved version code
        val prefs = getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE)
        val savedVersionCode = prefs.getInt(PREF_VERSION_CODE_KEY, DOESNT_EXIST)

        // Check for first run or upgrade
        when {
            currentVersionCode == savedVersionCode -> { // This is just a normal run
                val notificationPrefs = getSharedPreferences(
                        "blog.nordgedanken.simplematrix.status_notifications", Context.MODE_PRIVATE)
                val notificationID = notificationPrefs.getInt("normalSyncID", -1)
                if (notificationID != -1) {
                    val notificationManager = NotificationManagerCompat.from(this)
                    notificationManager.cancel(notificationID)
                }
                return
            }
            savedVersionCode == DOESNT_EXIST -> {
                // This is a new install (or the user cleared the shared preferences)
                // DO NOTHING
            }
            currentVersionCode > savedVersionCode -> {
                // This is an upgrade
                val notificationPrefs = getSharedPreferences(
                        "blog.nordgedanken.simplematrix.status_notifications", Context.MODE_PRIVATE)

                notificationPrefs.edit().putInt("normalSyncID", -1).commit()
            }
        }

        // Update the shared preferences with the current version code
        prefs.edit().putInt(PREF_VERSION_CODE_KEY, currentVersionCode).apply()
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    fun onAppBackgrounded() {
        //App in background
        appState = "background"
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    fun onAppForegrounded() {
        // App in foreground
        appState = "foreground"
    }
}

class DeepLinkReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context, intent: Intent) {
        val deepLinkUri = intent.getStringExtra(DeepLinkHandler.EXTRA_URI)
        if (intent.getBooleanExtra(DeepLinkHandler.EXTRA_SUCCESSFUL, false)) {
            Logger.i("Success deep linking: $deepLinkUri")
        } else {
            val errorMessage = intent.getStringExtra(DeepLinkHandler.EXTRA_ERROR_MESSAGE)
            Logger.e("Error deep linking: $deepLinkUri with error message +$errorMessage")
        }
    }
}