package blog.nordgedanken.simplematrix.roomListView

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import blog.nordgedanken.matrix_android_sdk.api.viewModels.RoomList
import blog.nordgedanken.simplematrix.R
import blog.nordgedanken.simplematrix.roomListView.recyclerView.RoomListController
import kotlinx.android.synthetic.main.activity_room_list.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class RoomListActivity : AppCompatActivity() {
    private val roomList: RoomList by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_room_list)

        val controller = RoomListController(this)

        recycler_view.setController(controller)

        roomList.getRooms().observe(this, Observer {
          controller.setData(it.chunk)
        })
    }
}
