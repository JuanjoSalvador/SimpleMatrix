package blog.nordgedanken.simplematrix.data.matrix.sync.initialSync

import android.accounts.AccountManager
import android.app.IntentService
import android.content.Intent
import android.content.SharedPreferences
import android.preference.PreferenceManager
import android.view.View
import androidx.core.content.ContextCompat
import blog.nordgedanken.matrix_android_sdk.account.Account
import blog.nordgedanken.simplematrix.MainActivity
import blog.nordgedanken.simplematrix.R
import blog.nordgedanken.simplematrix.data.db.AppDatabase
import blog.nordgedanken.simplematrix.data.matrix.MatrixClient
import blog.nordgedanken.simplematrix.data.matrix.Sync
import blog.nordgedanken.simplematrix.data.matrix.sync.SyncStarter
import blog.nordgedanken.simplematrix.data.matrix.sync.processing.SyncProcessing
import blog.nordgedanken.simplematrix.roomView.RoomsActivity
import blog.nordgedanken.simplematrix.utils.Connectivity
import blog.nordgedanken.simplematrix.utils.Notification
import blog.nordgedanken.simplematrix.utils.removeAccount
import com.google.gson.JsonObject
import com.google.gson.JsonPrimitive
import com.orhanobut.logger.Logger
import io.kamax.matrix.client.MatrixClientRequestException
import io.kamax.matrix.client._SyncData
import io.kamax.matrix.client.regular.SyncOptions
import io.kamax.matrix.json.GsonUtil
import io.sentry.Sentry
import io.sentry.event.BreadcrumbBuilder
import org.jetbrains.anko.runOnUiThread
import org.koin.android.ext.android.inject

const val ONGOING_NOTIFICATION_ID = 9998

/**
 * Created by MTRNord on 15.02.2019.
 */
class InitialSyncService : IntentService("Initial_Sync_Service") {

    private val account: Account by inject()
    private val db: AppDatabase by inject()
    private lateinit var sharedPreferences: SharedPreferences

    private enum class SyncErrors {
        FatalError,
        SomethingWentWrong,
        Empty,
        Finished,
    }

    fun sync(timeout: Long): _SyncData? {
        with(SyncOptions.build()) {
            setTimeout(timeout)
            val timeline = JsonObject()
            timeline.add("limit", JsonPrimitive(1))

            val roomFilter = JsonObject()
            roomFilter.add("timeline", timeline)

            val filter = JsonObject()
            filter.add("room", roomFilter)

            val jsonString = GsonUtil.get().toJson(filter)
            setFilter(jsonString)
            return account.client?.sync(get())
        }
    }

    override fun onCreate() {
        super.onCreate()
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this)

        val notification = Notification.buildForegroundServiceNotification(this, R.string.initialSync)

        this.startForeground(ONGOING_NOTIFICATION_ID, notification)
    }

    override fun onHandleIntent(intent: Intent?) {
        loop()
    }

    private fun loop() {
        this.runOnUiThread {
            RoomsActivity.loadProgressVisibility.value = View.VISIBLE
            RoomsActivity.syncStatusVisibility.value = View.VISIBLE
        }
        RoomsActivity.syncStatus.postValue(getString(R.string.starting_initial_sync))
        var retries = 0
        while (true) {
            if (Connectivity.isConnected(this)) {
                Logger.d("Starting sync, retries: $retries")
                RoomsActivity.syncStatus.postValue(getString(R.string.starting_initial_sync_with_retries, retries))
                val status = actualWork(db)
                if (status == SyncErrors.FatalError) {
                    stopSelf()
                } else if (status == SyncErrors.SomethingWentWrong) {
                    stopSelf()
                } else if( status == SyncErrors.Finished) {
                    break
                }
                retries++

                // No delay!
            }
        }
        SyncStarter.initialSyncDone.postValue(true)
        this.runOnUiThread {
            RoomsActivity.loadProgressVisibility.postValue(View.GONE)
            RoomsActivity.syncStatusVisibility.postValue(View.GONE)
        }
        SyncStarter.startSync(this)
        stopSelf()
    }

    // TODO return enum
    private fun actualWork(db: AppDatabase): SyncErrors {
        Sentry.getContext().recordBreadcrumb(
                BreadcrumbBuilder().setMessage("Initial Sync").build()
        )

        // Try initial Sync
        val syncData = try {
            sync(0)
        } catch (e: MatrixClientRequestException) {
            if(e.error.isPresent) {
                val error = e.error.get()
                if (error.errcode == "M_UNKNOWN_TOKEN") {
                    this.runOnUiThread {
                        RoomsActivity.syncStatus.value = getString(R.string.error, "Invalid access_token")
                    }
                    MatrixClient.client = null
                    val am = AccountManager.get(this)
                    val accounts = am?.getAccountsByType("org.matrix")
                    // TODO fix when using multiple accounts
                    removeAccount(account = accounts!![0], activity = null, handler = null, callback = null, accountManager = am)

                    // Let the User login again
                    val mainActivityIntent = Intent(this, MainActivity::class.java)
                    ContextCompat.startActivity(this, mainActivityIntent, null)
                    return SyncErrors.FatalError
                }
                Logger.d("ErrorCode: " + error.errcode + "\nErrorMessage: " + error.error)
            }
            null
        }

        if (syncData != null) {
            // Save the next Batch token for the next sync iteration
            if (db.syncDao().all.count() == 1) {
                val sync = db.syncDao().all[0]
                val token = syncData.nextBatchToken()!!
                sync.syncToken = token
                db.syncDao().updateAll(sync)
            } else {
                val sync = Sync()
                val token = syncData.nextBatchToken()!!
                sync.syncToken = token
                db.syncDao().insertAll(sync)
            }
            this.runOnUiThread {
                RoomsActivity.syncStatus.value = getString(R.string.processing_initial_sync_response)
            }

            SyncProcessing(syncData)
            return SyncErrors.Finished
        } else {
            Logger.d("No response!")
            return SyncErrors.Empty
        }
    }
}