/*
 * SimpleMatrix - A simplified Android Matrix Client
 * Copyright (C) 2018 Marcel Radzio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package blog.nordgedanken.simplematrix.data.view

import androidx.paging.DataSource
import androidx.room.Dao
import androidx.room.Query
import androidx.room.Transaction
import blog.nordgedanken.simplematrix.utils.db.BaseDao

/**
 * Created by MTRNord on 01.09.2018.
 */
@Dao
abstract class MessageDao : BaseDao<Message> {
    @Transaction
    @Query("SELECT * FROM messages")
    abstract fun all(): List<MessageFull>

    @Transaction
    @Query("SELECT id FROM messages")
    abstract fun allIDs(): List<String>

    @Transaction
    @Query("SELECT * FROM messages WHERE room_id = :roomID")
    abstract fun allByRoomID(roomID: String): DataSource.Factory<Int, MessageFull>

    @Transaction
    @Query("SELECT Count(*) FROM messages WHERE room_id = :roomID")
    abstract fun countByRoomID(roomID: String): Int

    @Transaction
    @Query("SELECT * FROM messages WHERE room_id = :roomID ORDER BY createdAt DESC")
    abstract fun allByRoomIDReverse(roomID: String): DataSource.Factory<Int, MessageFull>

    @Transaction
    @Query("SELECT * FROM messages WHERE room_id = :roomID ORDER BY createdAt DESC LIMIT 1;")
    abstract fun latestMessage(roomID: String): MessageFull

    @Transaction
    @Query("SELECT * FROM messages WHERE id IN (:messageIds)")
    abstract fun loadAllByIds(messageIds: IntArray): List<MessageFull>

    @Transaction
    @Query("SELECT * FROM messages WHERE room_id = :roomID")
    abstract fun loadAllByRoomId(roomID: String): List<MessageFull>

    @Transaction
    @Query("Update messages SET id = :newID WHERE id = :oldID")
    abstract fun updateMessageID(oldID: String, newID: String)

    @Transaction
    @Query("Update messages SET sending_in_process = :status WHERE id = :id")
    abstract fun updateMessageSendingStatus(id: String, status: Boolean)

    @Query("DELETE FROM messages")
    abstract fun nukeTable()
}
