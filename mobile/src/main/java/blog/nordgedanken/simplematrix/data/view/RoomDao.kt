/*
 * SimpleMatrix - A simplified Android Matrix Client
 * Copyright (C) 2018 Marcel Radzio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package blog.nordgedanken.simplematrix.data.view

import androidx.paging.DataSource
import androidx.room.Dao
import androidx.room.Query
import androidx.room.Transaction
import blog.nordgedanken.simplematrix.utils.db.BaseDao

/**
 * Created by MTRNord on 01.09.2018.
 */
@Dao
abstract class RoomDao : BaseDao<Room> {
    @Transaction
    @Query("SELECT * FROM rooms")
    abstract fun all(): List<RoomFull>

    @Transaction
    @Query("SELECT EXISTS(SELECT 1 FROM rooms WHERE id=:roomID LIMIT 1);")
    abstract fun exists(roomID: String): Boolean

    @Transaction
    @Query("SELECT id FROM rooms")
    abstract fun allIDs(): List<String>

    @Transaction
    @Query("SELECT count(*) FROM rooms")
    abstract fun allCount(): Int

    @Transaction
    @Query("SELECT * FROM rooms WHERE tag == 'favs'")
    abstract fun allFavs(): List<RoomFull>

    @Transaction
    @Query("SELECT * FROM rooms WHERE tag == 'direct'")
    abstract fun allDirects(): List<RoomFull>

    @Transaction
    @Query("SELECT * FROM rooms WHERE tag == 'chats'")
    abstract fun allChats(): List<RoomFull>

    @Transaction
    @Query("SELECT * FROM rooms WHERE tag == 'favs'")
    abstract fun allFavsPaged(): DataSource.Factory<Int, RoomFull>

    @Transaction
    @Query("SELECT * FROM rooms WHERE tag == 'direct'")
    abstract fun allDirectPaged(): DataSource.Factory<Int, RoomFull>

    @Transaction
    @Query("SELECT * FROM rooms WHERE tag == 'chats'")
    abstract fun allChatsPaged(): DataSource.Factory<Int, RoomFull>

    @Transaction
    @Query("SELECT * FROM rooms WHERE id IN (:roomIds)")
    abstract fun loadAllByIds(roomIds: Array<String>): List<RoomFull>

    @Transaction
    @Query("SELECT * FROM rooms WHERE id = :roomId LIMIT 1")
    abstract fun roomByID(roomId: String): RoomFull?

    @Transaction
    @Query("SELECT id FROM rooms WHERE id = :roomId LIMIT 1")
    abstract fun roomIDByID(roomId: String): String

    @Transaction
    @Query("DELETE FROM rooms WHERE id = :roomId")
    abstract fun deleteByID(roomId: String)

    @Transaction
    @Query("DELETE FROM rooms")
    abstract fun nukeTable()
}