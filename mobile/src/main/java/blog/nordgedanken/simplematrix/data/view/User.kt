/*
 * SimpleMatrix - A simplified Android Matrix Client
 * Copyright (C) 2018 Marcel Radzio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package blog.nordgedanken.simplematrix.data.view

import androidx.core.app.Person
import androidx.core.graphics.drawable.IconCompat
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import blog.nordgedanken.simplematrix.utils.NameAvatar

@Entity(tableName = "users")
class User {

    @PrimaryKey(autoGenerate = true)
    var id: Int? = null
    @ColumnInfo(name = "mxid")
    var MXID: String? = null
    @ColumnInfo(name = "room_id")
    var roomID: String? = null
    @ColumnInfo(name = "name")
    var name: String = ""
    @ColumnInfo(name = "avatar")
    var avatar: String = ""
    @ColumnInfo(name = "isOnline")
    var isOnline: Boolean? = false
    @Transient
    var nameAvatar: NameAvatar? = null

    fun getPerson(): Person {
        val iconCompat = IconCompat.createWithContentUri(avatar)
        return Person.Builder().setBot(false)
                .setName(name)
                .setIcon(iconCompat)
                .build()
    }

}