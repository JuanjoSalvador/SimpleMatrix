package blog.nordgedanken.simplematrix.data.matrix.sync.processing

import blog.nordgedanken.simplematrix.data.view.Room
import blog.nordgedanken.simplematrix.data.view.RoomFull
import blog.nordgedanken.simplematrix.utils.db.DBHelpers
import blog.nordgedanken.simplematrix.utils.db.DBHelpers.getRoomByID
import com.orhanobut.logger.Logger
import io.kamax.matrix.event._MatrixAccountDataEvent
import io.kamax.matrix.json.event.MatrixJsonDirectEvent

/**
 * GlobalAccountData processes any global account_data from the sync
 *
 * @author  Marcel Radzio
 * @since   2018-10-28
 *
 * @param data means all the account_data which came using the Sync
 * and needs to be a [List] of type [io.kamax.matrix.event._MatrixAccountDataEvent]
 *
 * @param context needs to be of type [android.content.Context] and is used mainly for UI tasks
 */
class GlobalAccountData(data: List<_MatrixAccountDataEvent>) {
    /**
     * init is the entry of this class and gets used to split the data into work chunks
     */
    init {
        // Parse m.direct events
        for (event in data.mapNotNull { parseEvent(it.json) }) {
            when (event) {
                is MatrixJsonDirectEvent -> {
                    processDirect(event)
                }
            }
        }
    }

    /**
     * processDirect processes any matrix event of type "m.direct" from the sync
     *
     * @param event means one "m.direct" event which which came using the Sync
     * and needs to be of type [io.kamax.matrix.json.event.MatrixJsonDirectEvent]
     */
    private fun processDirect(event: MatrixJsonDirectEvent) {
        val rooms = mutableListOf<RoomFull>()

        // Double loop is required to ensure that a room is created before processing the data
        // As this might break with huge data sets TODO is needed.
        for (userObject in event.json["content"].asJsonObject.entrySet()) {
            val roomArray = userObject.value.asJsonArray

            for (roomIDJSON in roomArray) {
                val roomId = roomIDJSON.asString
                if (!DBHelpers.roomExistsInCache(roomId)) {
                    val roomPOJO = RoomFull()
                    roomPOJO.room = Room()
                    roomPOJO.room?.id = roomId
                    rooms.add(roomPOJO)
                }
            }
        }
        DBHelpers.saveRoomToDB(rooms)

        // Actual processing
        rooms.clear()
        Logger.d(event.json["content"])
        for (userObject in event.json["content"].asJsonObject.entrySet()) {
            Logger.d(userObject)
            // TODO save actual user_id somewhere
            val user_id = userObject.key
            val roomArray = userObject.value.asJsonArray
            for (roomIDJSON in roomArray) {
                val roomID = roomIDJSON.asString

                // We can save assume that the room exists in the DB
                val room: RoomFull = getRoomByID(roomID)
                room.room?.tag = "direct"
                rooms.add(room)
            }
        }
        DBHelpers.saveRoomToDB(rooms)
    }
}