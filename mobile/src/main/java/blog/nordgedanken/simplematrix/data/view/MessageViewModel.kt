package blog.nordgedanken.simplematrix.data.view

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.paging.Config
import androidx.paging.PagedList
import androidx.paging.toLiveData
import blog.nordgedanken.simplematrix.data.db.AppDatabase

class MessageViewModel(app: Application) : AndroidViewModel(app) {
    private val dao = AppDatabase.getInstance(app).messageDao()

    /**
     * We use -ktx Kotlin extension functions here, otherwise you would use LivePagedListBuilder(),
     * and PagedList.Config.Builder()
     */
    fun messageList(roomID: String): LiveData<PagedList<MessageFull>> {
        return dao.allByRoomIDReverse(roomID).toLiveData(Config(
                /**
                 * A good page size is a value that fills at least a screen worth of content on a large
                 * device so the User is unlikely to see a null item.
                 * You can play with this constant to observe the paging behavior.
                 * <p>
                 * It's possible to vary this with list device size, but often unnecessary, unless a
                 * user scrolling on a large device is expected to scroll through items more quickly
                 * than a small device, such as when the large device uses a grid layout of items.
                 */
                pageSize = 10,

                enablePlaceholders = true,

                /**
                 * Maximum number of items a PagedList should hold in memory at once.
                 *
                 *
                 * This number triggers the PagedList to start dropping distant pages as more are loaded.
                 */
                maxSize = 30))
    }
}