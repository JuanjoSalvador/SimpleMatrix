package blog.nordgedanken.simplematrix.data.matrix.sync.processing

import io.kamax.matrix.client._SyncData

/**
 * InvitedRooms processes any invited rooms from the Sync
 *
 * @author  Marcel Radzio
 * @since   2018-10-28
 *
 * @param data Data means all the data of invited rooms which came using the Sync
 * and needs to be a [Set] of type [io.kamax.matrix.client._SyncData.InvitedRoom]
 */
class InvitedRooms(data: Set<_SyncData.InvitedRoom>) {

  /**
   * init is the entry of this class and gets used to split the data into work chunks
   */
  init {
  }
}