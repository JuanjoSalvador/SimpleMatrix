package blog.nordgedanken.simplematrix.data.matrix.sync.processing

import blog.nordgedanken.simplematrix.data.matrix.additionaltypes.messages.MatrixJsonRoomImageMessageEvent
import blog.nordgedanken.simplematrix.data.view.Message
import blog.nordgedanken.simplematrix.data.view.MessageFull
import blog.nordgedanken.simplematrix.data.view.RoomFull
import blog.nordgedanken.simplematrix.data.view.User
import blog.nordgedanken.simplematrix.utils.db.DBHelpers.getRoomByID
import blog.nordgedanken.simplematrix.utils.db.DBHelpers.getUsersByRoomIDFromCache
import blog.nordgedanken.simplematrix.utils.db.DBHelpers.saveMessagesToDB
import blog.nordgedanken.simplematrix.utils.db.DBHelpers.saveRoomToDB
import blog.nordgedanken.simplematrix.utils.db.DBHelpers.saveUserToDB
import com.orhanobut.logger.Logger
import io.kamax.matrix.client._SyncData
import io.kamax.matrix.event._MatrixAccountDataEvent
import io.kamax.matrix.event._MatrixPersistentEvent
import io.kamax.matrix.json.event.*
import java.util.*

/**
 * JoinedRooms processes any joined rooms from the Sync.
 *
 * @author  Marcel Radzio
 * @since   2018-10-28
 *
 * @param data All the data of joined rooms which came using the Sync
 * and needs to be a [Set] of type [io.kamax.matrix.client._SyncData.JoinedRoom]
 */
class JoinedRooms(data: Set<_SyncData.JoinedRoom>? = null) {
    private val rooms = mutableListOf<RoomFull>()

    /**
     * The entry of this class and gets used to split the data into work chunks.
     */
    init {
        if (data != null) {
            for (chunk in data) {
                // TODO more types available
                val stateEvents = chunk.state.events
                val currentRoom = getRoomByID(chunk.id)
                currentRoom.room?.prevBatchToken = chunk.timeline.previousBatchToken ?: ""
                val messages = mutableListOf<MessageFull?>()

                if (stateEvents.isNotEmpty()) {
                    processState(stateEvents, currentRoom)
                }
                val timelineEvents = chunk.timeline.events
                if (timelineEvents.isNotEmpty()) {
                    // We can save assume that the room exists in the DB
                    processTimeline(timelineEvents, currentRoom, messages)
                }
                val accountDataEvents = chunk.accountData.events
                if (accountDataEvents.isNotEmpty()) {
                    processAccountData(accountDataEvents, currentRoom)
                }
                saveMessagesToDB(messages.filterNotNull())
                rooms.add(currentRoom)
            }
            saveRoomToDB(rooms)
        }
    }

    /**
     * Splits the Timeline Events into smaller event based work chunks.
     *
     * @param events All timeline events
     */
    fun processTimeline(events: List<_MatrixPersistentEvent>, room: RoomFull, messages: MutableList<MessageFull?>) {
        val parsedEvents = events.mapNotNull { parseEvent(it.json) }
        val users = mutableListOf<User?>()
        for (event in parsedEvents) {
            when (event) {
                is MatrixJsonRoomMembershipEvent -> users.add(processMembership(event, room))
                is MatrixJsonRoomTopicEvent -> processTopic(event, room)
                is MatrixJsonRoomNameEvent -> processName(event, room)
                is MatrixJsonRoomAliasesEvent -> room.room?.aliases = event.aliases
                is MatrixJsonRoomCanonicalAliasEvent -> room.room?.canonicalAlias = event.alias.orElseGet { "" }
                is MatrixJsonRoomAvatarEvent -> processAvatar(event, room)
                is MatrixJsonRoomMessageEvent -> messages.add(processMessages(event, room))
            }
        }
        saveUserToDB(users.filterNotNull())
    }

    /**
     * Splits the State Events into smaller event based work chunks.
     *
     * @param events All state events
     */
    private fun processState(events: List<_MatrixPersistentEvent>, room: RoomFull) {
        val parsedEvents = events.mapNotNull { parseEvent(it.json) }
        val users = mutableListOf<User?>()
        for (event in parsedEvents) {
            when (event) {
                is MatrixJsonRoomMembershipEvent -> users.add(processMembership(event, room))
                is MatrixJsonRoomTopicEvent -> processTopic(event, room)
                is MatrixJsonRoomNameEvent -> processName(event, room)
                is MatrixJsonRoomAliasesEvent -> room.room?.aliases = event.aliases
                is MatrixJsonRoomCanonicalAliasEvent -> room.room?.canonicalAlias = event.alias.orElseGet { "" }
                is MatrixJsonRoomAvatarEvent -> processAvatar(event, room)
            }
        }
        saveUserToDB(users.filterNotNull())
    }

    /**
     * Splits the AccountDta Events into smaller event based work chunks.
     *
     * @param events All AccountData events
     */
    private fun processAccountData(events: List<_MatrixAccountDataEvent>, room: RoomFull) {
        val parsedEvents = events.mapNotNull { parseEvent(it.json) }
        for (event in parsedEvents) {
            when (event) {
                is MatrixJsonRoomTagsEvent -> processTag(event, room)
            }
        }
    }

    /**
     * Adds the Topic to a [RoomFull] object.
     *
     * @param event A single Topic Event
     */
    private fun processTopic(event: MatrixJsonRoomTopicEvent, room: RoomFull) {
        if (event.topic.isPresent) {
            // Set the new topic. We can save assume this is a newer Event
            room.room?.topic = event.topic.get()
        }
    }

    /**
     * Adds the Room Name to a [RoomFull] object.
     *
     * @param event A single Name Event
     */
    private fun processName(event: MatrixJsonRoomNameEvent, room: RoomFull) {
        if (event.name.isPresent) {
            // Set the new name. We can save assume this is a newer Event
            room.room?.dialogName = event.name.get()
        }
    }

    /**
     * Adds the Room Avatar to a [RoomFull] object.
     *
     * @param event A single Avatar Event
     */
    private fun processAvatar(event: MatrixJsonRoomAvatarEvent, room: RoomFull) {
        if (event.url != null) {
            // Set the new avatarURL. We can save assume this is a newer Event
            room.room?.dialogPhoto = event.url
        }
    }

    /**
     * Adds a Room Message to a [RoomFull] object.
     *
     * @param event A single Avatar Event
     */
    private fun processMessages(event: MatrixJsonRoomMessageEvent, room: RoomFull): MessageFull? {
        val messageSmall = Message()
        val message = MessageFull()
        message.message = messageSmall

        val imageEvent = if (event.bodyType == "m.image") {
            MatrixJsonRoomImageMessageEvent(event.json)
        } else {
            null
        }

        message.message?.id = event.id
        message.message?.roomID = room.room?.id

        val format = event.format
        val formattedBody = event.formattedBody

        message.message?.image = if (imageEvent != null && imageEvent.url.isPresent) {
            imageEvent.url.get()
        } else {
            ""
        }
        message.message?.imageHeight = if (imageEvent != null && imageEvent.height.isPresent) {
            imageEvent.height.get()
        } else {
            null
        }
        message.message?.imageWidth = if (imageEvent != null && imageEvent.width.isPresent) {
            imageEvent.width.get()
        } else {
            null
        }

        var possibleRedaction = false
        when {
            format.isPresent && format.get() == "org.matrix.custom.html"
                    && formattedBody.isPresent -> {
                message.message?.textHTML = formattedBody.get()
                try {
                    message.message?.text = event.body
                } catch (e: NullPointerException) {
                    possibleRedaction = true
                    Logger.d("NullPointer while getting body: ${event.json}")
                    // TODO find out, why NPEs can happen. Reproduce by loading backlog in #test:matrix.org
                }
            }
            else -> try {
                message.message?.text = event.body
            } catch (e: NullPointerException) {
                possibleRedaction = true
                Logger.d("NullPointer while getting body: ${event.json}")
                // TODO find out, why NPEs can happen. Reproduce by loading backlog in #test:matrix.org
            }
        }
        message.message?.userID = event.sender.id
        message.message?.createdAt = Date(event.time)
        if (!possibleRedaction) {
            return message
        }
        return null
    }

    /**
     * Sets the Room Tag in a [RoomFull] object.
     *
     * @param event A Tag Avatar Event
     */
    private fun processTag(event: MatrixJsonRoomTagsEvent, room: RoomFull) {
        if (event.tags.isNotEmpty()) {
            val favTag = event.tags.asSequence().filter { tag -> "m" == tag.namespace && "favourite" == tag.name }.firstOrNull()
            if (favTag != null) {
                room.room?.tag = "favs"
            }
        }
    }

    /**
     * Adds or removes a User to a [RoomFull] object.
     *
     * @param event A single Membership Event
     */
    private fun processMembership(event: MatrixJsonRoomMembershipEvent, room: RoomFull): User? {
        val membershipType = event.membership
        val id = event.invitee

        when (membershipType) {
            "join" -> {
                val dbUser = getUsersByRoomIDFromCache(room.room?.id!!)
                val userFromCache = dbUser.firstOrNull { it.MXID == id.id }

                return if (userFromCache != null) {
                    // We seem to already know the User. We just need to update it.
                    if (event.displayName.isPresent) userFromCache.name = event.displayName.get()
                    if (event.avatarUrl.isPresent) userFromCache.avatar = event.avatarUrl.get()
                    userFromCache
                } else {
                    // We seem to not know the User. We create a new one
                    val user = User()
                    if (event.displayName.isPresent) user.name = event.displayName.get()
                    if (event.avatarUrl.isPresent) user.avatar = event.avatarUrl.get()
                    user.roomID = room.room?.id
                    user.MXID = id.id
                    user
                }
            }
            "leave" -> {
            }
            "ban" -> {
            }
            "invite" -> {
            }
        }
        return null
    }
}