/*
 * SimpleMatrix - A simplified Android Matrix Client
 * Copyright (C) 2018 Marcel Radzio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package blog.nordgedanken.simplematrix.data.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import androidx.room.migration.Migration
import androidx.sqlite.db.SupportSQLiteDatabase
import blog.nordgedanken.simplematrix.data.matrix.Sync
import blog.nordgedanken.simplematrix.data.matrix.SyncDao
import blog.nordgedanken.simplematrix.data.view.*
import blog.nordgedanken.simplematrix.utils.converters.Converters

/**
 * Created by MTRNord on 01.09.2018.
 */
@Database(entities = [Message::class, blog.nordgedanken.simplematrix.data.view.Room::class, User::class, Sync::class], version = 12)
@TypeConverters(Converters::class)
abstract class AppDatabase : RoomDatabase() {
    abstract fun messageDao(): MessageDao
    abstract fun roomDao(): RoomDao
    abstract fun userDao(): UserDao
    abstract fun syncDao(): SyncDao

    companion object {
        /**
         * The only instance
         */
        private var sInstance: AppDatabase? = null

        private val MIGRATION_9_10 = object : Migration(9, 10) {
            override fun migrate(database: SupportSQLiteDatabase) {
                // BROKEN! SKIP!
                /*database.execSQL("CREATE INDEX rooms_name ON rooms (dialogName)")
                database.execSQL("CREATE INDEX rooms_notification ON rooms (notificationID)")
                database.execSQL("CREATE INDEX rooms_tag ON rooms (tag)")*/
            }
        }

        private val MIGRATION_10_11 = object : Migration(10, 11) {
            override fun migrate(database: SupportSQLiteDatabase) {
                // BROKEN! SKIP!
                /*database.execSQL("CREATE INDEX index_rooms_id_dialogName ON rooms (id, dialogName)")
                database.execSQL("CREATE INDEX index_rooms_id_notificationID ON rooms (id, notificationID)")
                database.execSQL("CREATE INDEX index_rooms_id_tag ON rooms (id, tag)")*/
            }
        }

        private val MIGRATION_11_12 = object : Migration(11, 12) {
            override fun migrate(database: SupportSQLiteDatabase) {
                database.execSQL("ALTER TABLE rooms ADD COLUMN prevBatchToken TEXT")
            }
        }

        /**
         * Gets the singleton instance of SampleDatabase.
         *
         * @param context The context.
         * @return The singleton instance of AppDatabase.
         */
        @Synchronized
        fun getInstance(context: Context): AppDatabase {
            if (sInstance == null) {
                sInstance = Room
                        .databaseBuilder(context.applicationContext, AppDatabase::class.java, "cache")
                        .fallbackToDestructiveMigration()
                        //.addMigrations(MIGRATION_9_10, MIGRATION_10_11, MIGRATION_11_12)
                        .build()
            }
            return sInstance!!
        }
    }
}