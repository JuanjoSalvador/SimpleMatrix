package blog.nordgedanken.simplematrix.data.matrix.additionaltypes.messages;

import com.google.gson.JsonObject;

import io.kamax.matrix.json.GsonUtil;
import io.kamax.matrix.json.event.MatrixJsonRoomMessageEvent;
import java8.util.Optional;

public class MatrixJsonRoomImageMessageEvent extends MatrixJsonRoomMessageEvent {
    public MatrixJsonRoomImageMessageEvent(JsonObject obj) {
        super(obj);
        this.content = obj;
    }

    public Optional<String> getURL() {
        return GsonUtil.findString(this.content, "url");
    }

    public Optional<JsonObject> getInfo() {
        return GsonUtil.findObj(this.content, "info");
    }

    public Optional<JsonObject> getThumbnailInfo() {
        Optional<JsonObject> info = this.getInfo();
        if (info.isPresent()) {
            return GsonUtil.findObj(info.get(), "thumbnail_info");
        }
        return Optional.empty();
    }

    public Optional<Integer> getWidth() {
        Optional<JsonObject> info = this.getInfo();
        if (info.isPresent()) {
            Optional<JsonObject> thumbnail_info = this.getThumbnailInfo();
            if (thumbnail_info.isPresent()) {
                Optional<Long> width = GsonUtil.findLong(info.get(), "w");
                if (width.isPresent()) {
                    int width_int = width.get().intValue();
                    return Optional.ofNullable(width_int);
                }
            }
        }
        return Optional.empty();
    }

    public Optional<Integer> getHeight() {
        Optional<JsonObject> info = this.getInfo();
        if (info.isPresent()) {
            Optional<JsonObject> thumbnail_info = this.getThumbnailInfo();
            if (thumbnail_info.isPresent()) {
                Optional<Long> width = GsonUtil.findLong(info.get(), "h");
                if (width.isPresent()) {
                    int width_int = width.get().intValue();
                    return Optional.ofNullable(width_int);
                }
            }
        }
        return Optional.empty();
    }
}