/*
 * SimpleMatrix - A simplified Android Matrix Client
 * Copyright (C) 2018 Marcel Radzio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package blog.nordgedanken.simplematrix.data.matrix

import android.content.Context
import android.view.View
import blog.nordgedanken.simplematrix.data.db.AppDatabase
import blog.nordgedanken.simplematrix.data.matrix.sync.SyncStarter
import blog.nordgedanken.simplematrix.roomView.RoomsActivity
import blog.nordgedanken.simplematrix.utils.AppUtils
import blog.nordgedanken.simplematrix.utils.db.DBHelpers.getUsersFromCache
import com.facebook.stetho.okhttp3.StethoInterceptor
import com.orhanobut.logger.Logger
import io.kamax.matrix._MatrixID
import io.kamax.matrix._MatrixUser
import io.kamax.matrix.client.MatrixClientContext
import io.kamax.matrix.client.MatrixClientRequestException
import io.kamax.matrix.client.MatrixPasswordCredentials
import io.kamax.matrix.client.regular.MatrixHttpClient
import io.sentry.Sentry
import io.sentry.event.BreadcrumbBuilder
import okhttp3.OkHttpClient
import org.jetbrains.anko.runOnUiThread
import java.net.MalformedURLException
import java.net.URL
import java.net.UnknownHostException
import java.util.concurrent.TimeUnit


class MatrixClient {

    enum class LoginResult {
        Success, UnknownError, ServerNotFound, WrongPasswordOrUserNotFound, WellKnownURLNotFound
    }

    companion object {
        var client: MatrixHttpClient? = null
        var token: String? = null
        var cache = true
        var id: String? = null
        var name: String? = null
        var avatar_url: String? = null
    }


    private var okhttpClient: OkHttpClient.Builder? = null

    fun relogin(baseUrl: String, domain: String, id: _MatrixID) {
        okhttpClient = OkHttpClient.Builder()
                .addNetworkInterceptor(StethoInterceptor())
                .connectTimeout(30 * 1000, TimeUnit.MILLISECONDS)
                .writeTimeout(5, TimeUnit.MINUTES)
                .readTimeout(5, TimeUnit.MINUTES).followRedirects(false)
        val context = MatrixClientContext()
        context.initialDeviceName = "SimpleMatrix"
        val url = try {
            URL(baseUrl)
        } catch (e: MalformedURLException) {
            URL("https://$baseUrl")
        }
        context.hsBaseUrl = url
        context.domain = domain
        context.setUser(id)
        context.token = token
        client = MatrixHttpClient(context, okhttpClient)
    }

    fun loginWellKnown(contextA: Context, mxid: _MatrixID, password: String): Pair<String, MatrixClient.LoginResult> {
        okhttpClient = OkHttpClient.Builder()
                .addNetworkInterceptor(StethoInterceptor())
                .connectTimeout(30 * 1000, TimeUnit.MILLISECONDS)
                .writeTimeout(5, TimeUnit.MINUTES)
                .readTimeout(5, TimeUnit.MINUTES).followRedirects(false)

        val context = MatrixClientContext()
        context.initialDeviceName = "SimpleMatrix"
        context.domain = mxid.domain
        client = MatrixHttpClient(context, okhttpClient)

        try {
            client?.discoverSettings()
        } catch (e: IllegalStateException) {
            if (e.message == "No valid Homeserver base URL was found") {
                return Pair("", MatrixClient.LoginResult.WellKnownURLNotFound)
            }
        }

        val credentials = MatrixPasswordCredentials(mxid.localPart, password)

        try {
            client?.login(credentials)
        } catch (e: MatrixClientRequestException) {
            var loginResult = MatrixClient.LoginResult.UnknownError
            if (e.cause is UnknownHostException) {
                loginResult = MatrixClient.LoginResult.ServerNotFound
            }
            Logger.d(e)
            Logger.d(e.error)
            val error = e.error
            if (error.isPresent) {
                Logger.e(error.get().errcode + " :" + error.get().error)
                if (error.get().errcode == "M_FORBIDDEN" && error.get().error == "Invalid password") {
                    loginResult = MatrixClient.LoginResult.WrongPasswordOrUserNotFound
                }
            }

            return Pair("", loginResult)
        } catch (e: IllegalStateException) {
            Logger.e("Login unsuccessful", e)
            contextA.runOnUiThread {
                AppUtils.showToast(contextA, "Login unsuccessful", true)
            }
            return Pair("", MatrixClient.LoginResult.UnknownError)
        }

        return Pair(client!!.accessToken.get(), MatrixClient.LoginResult.Success)
    }

    fun login(contextA: Context, mxid: _MatrixID, server: String, password: String): Pair<String, MatrixClient.LoginResult> {
        okhttpClient = OkHttpClient.Builder()
                .addNetworkInterceptor(StethoInterceptor())
                .connectTimeout(30 * 1000, TimeUnit.MILLISECONDS)
                .writeTimeout(5, TimeUnit.MINUTES)
                .readTimeout(5, TimeUnit.MINUTES).followRedirects(false)
        val context = MatrixClientContext()
        val url = try {
            URL(server)
        } catch (e: MalformedURLException) {
            URL("https://$server")
        }
        context.hsBaseUrl = url
        context.initialDeviceName = "SimpleMatrix"
        client = MatrixHttpClient(context, okhttpClient)

        val credentials = MatrixPasswordCredentials(mxid.localPart, password)

        try {
            client!!.context?.initialDeviceName = "SimpleMatrix"
            client!!.login(credentials)
        } catch (e: MatrixClientRequestException) {
            var loginResult = MatrixClient.LoginResult.UnknownError
            if (e.cause is UnknownHostException) {
                loginResult = MatrixClient.LoginResult.ServerNotFound
            }
            Logger.d(e)
            Logger.d(e.error)
            val error = e.error
            if (error.isPresent) {
                Logger.e(error.get().errcode + " :" + error.get().error)
                if (error.get().errcode == "M_FORBIDDEN" && error.get().error == "Invalid password") {
                    loginResult = MatrixClient.LoginResult.WrongPasswordOrUserNotFound
                }
            }

            return Pair("", loginResult)
        } catch (e: IllegalStateException) {
            Logger.e("Login unsuccessful", e)
            contextA.runOnUiThread {
                AppUtils.showToast(contextA, "Login unsuccessful", true)
            }
            return Pair("", MatrixClient.LoginResult.UnknownError)
        }

        return Pair(client!!.accessToken.get(), MatrixClient.LoginResult.Success)
    }

    fun loadCache(context: Context) {
        Sentry.getContext().recordBreadcrumb(
                BreadcrumbBuilder().setMessage("Started loading cache").build()
        )

        // Setup user if empty
        val knownUser = MatrixClient.client?.user?.get()
        id = knownUser?.id
        val user = getUsersFromCache().firstOrNull { user -> user.MXID == id }
        name = user?.name
        avatar_url = user?.avatar
        var mxUser: _MatrixUser? = null
        if (name == null || avatar_url == null || name?.isEmpty() == true || avatar_url?.isEmpty() == true) {
            mxUser = MatrixClient.client?.getUser(knownUser)
        }
        if (name == null || name?.isEmpty() == true) {
            name = mxUser?.name?.get()!!
        }
        if (avatar_url == null || avatar_url?.isEmpty() == true) {
            avatar_url = mxUser?.avatarUrl?.orElse("")!!
        }

        val roomsCount = AppDatabase.getInstance(context).roomDao().allCount()

        cache = false
        if (roomsCount == 0) {
            SyncStarter.startInitialSync(context)
        } else {
            context.runOnUiThread {
                RoomsActivity.syncStatusVisibility.value = View.GONE
                RoomsActivity.loadProgressVisibility.value = View.GONE
                SyncStarter.initialSyncDone.value = true
            }
            Sentry.getContext().recordBreadcrumb(
                    BreadcrumbBuilder().setMessage("Cache loading finished").build()
            )
            SyncStarter.startSync(context)
        }
    }
}