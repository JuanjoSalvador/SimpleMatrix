/*
 * SimpleMatrix - A simplified Android Matrix Client
 * Copyright (C) 2018 Marcel Radzio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package blog.nordgedanken.simplematrix.data.view

import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey
import blog.nordgedanken.simplematrix.utils.NameAvatar
import io.kamax.matrix.event._MatrixEvent
import java.util.*

@Entity(tableName = "rooms", indices = [Index(value = ["id", "dialogName"]), Index(value = ["id", "notificationID"]), Index(value = ["id", "tag"])])
class Room {

    @PrimaryKey
    @NonNull
    var id: String = ""
    @ColumnInfo(name = "dialogName") var dialogName: String = ""
    @ColumnInfo(name = "dialogPhoto") var dialogPhoto: String = ""
    @ColumnInfo(name = "unreadCount") var unreadCount: Int = 0
    @ColumnInfo(name = "topic") var topic: String = ""
    @ColumnInfo(name = "aliases") var aliases: MutableList<String> = mutableListOf()
    @ColumnInfo(name = "canonical_alias") var canonicalAlias: String = ""
    @ColumnInfo(name = "fromToken") var fromToken: String = ""
    @ColumnInfo(name = "prevBatchToken") var prevBatchToken: String = ""
    @ColumnInfo(name = "tag")
    var tag: String? = "chats"
    @ColumnInfo(name = "notificationID") var notificationID: Int? = null
    @Transient var unprocessedBacklogEvents: ArrayList<_MatrixEvent>? = null
    @Transient var nameAvatar: NameAvatar? = null

    fun getFirstAlias(): String {
        if (aliases.isNotEmpty()) {
            return aliases[0]
        }
        return ""
    }
    fun addUnprocessedBacklogEvents(events: ArrayList<_MatrixEvent>) {
        if (this.unprocessedBacklogEvents == null) {
            this.unprocessedBacklogEvents = events
        } else {
            this.unprocessedBacklogEvents?.addAll(events)
        }
    }

    fun removeEvent(event: _MatrixEvent) {
        unprocessedBacklogEvents!!.remove(event)
    }
}