package blog.nordgedanken.simplematrix.chatView.recyclerView

import android.content.Context
import android.content.Intent
import androidx.core.content.ContextCompat
import blog.nordgedanken.simplematrix.*
import blog.nordgedanken.simplematrix.data.matrix.MatrixClient
import blog.nordgedanken.simplematrix.data.view.MessageFull
import com.airbnb.epoxy.EpoxyModel
import com.airbnb.epoxy.paging.PagedListEpoxyController
import io.sentry.Sentry
import io.sentry.event.BreadcrumbBuilder

class PagingMessageController(val context: Context) : PagedListEpoxyController<MessageFull>() {
    override fun buildItemModel(currentPosition: Int, item: MessageFull?): EpoxyModel<*> {
        return if (item?.message?.userID == MatrixClient.id) {
            if (item?.message?.image == "") {
                OutgoingTextMessageLayoutElementBindingModel_()
                        .id(item.message?.id)
                        .message(item)
            } else {
                return OutgoingImageMessageLayoutElementBindingModel_()
                        .id(item?.message?.id)
                        .message(item)
            }
        } else {
            if (item?.message?.image == "") {
                IncomingTextMessageLayoutElementBindingModel_()
                        .id(item.message?.id)
                        .message(item)
                        .onUserClick { _ ->
                            Sentry.getContext().recordBreadcrumb(
                                    BreadcrumbBuilder().setMessage("User is going to open a User Details").build()
                            )
                            val intent = Intent(context, UserProfile::class.java).putExtra("MXID", item.getUser().MXID).putExtra("roomID", item.message?.roomID)
                            // Swap without transition
                            ContextCompat.startActivity(context, intent,
                                    null)
                        }
            } else {
                return IncomingImageMessageLayoutElementBindingModel_()
                        .id(item?.message?.id)
                        .message(item)
                        .onUserClick { _ ->
                            Sentry.getContext().recordBreadcrumb(
                                    BreadcrumbBuilder().setMessage("User is going to open a User Details").build()
                            )
                            val intent = Intent(context, UserProfile::class.java).putExtra("MXID", item?.getUser()?.MXID).putExtra("roomID", item?.message?.roomID)
                            // Swap without transition
                            ContextCompat.startActivity(context, intent,
                                    null)
                        }
            }
        }
    }

    init {
        isDebugLoggingEnabled = true
    }

    override fun onExceptionSwallowed(exception: RuntimeException) {
        throw exception
    }
}