/*
 * SimpleMatrix - A simplified Android Matrix Client
 * Copyright (C) 2018 Marcel Radzio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package blog.nordgedanken.simplematrix.chatView

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import blog.nordgedanken.simplematrix.R
import blog.nordgedanken.simplematrix.chatView.recyclerView.PagingMessageController
import blog.nordgedanken.simplematrix.data.matrix.MatrixClient
import blog.nordgedanken.simplematrix.data.view.MessageViewModel
import blog.nordgedanken.simplematrix.data.view.RoomFull
import blog.nordgedanken.simplematrix.databinding.ActivityChatRoomBinding
import blog.nordgedanken.simplematrix.utils.ImageLoader
import blog.nordgedanken.simplematrix.utils.db.DBHelpers.getRoomByID
import com.airbnb.epoxy.EpoxyRecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.RequestManager
import com.google.android.material.navigation.NavigationView
import com.orhanobut.logger.Logger
import io.kamax.matrix.hs._MatrixRoom
import io.sentry.Sentry
import io.sentry.event.BreadcrumbBuilder
import kotlinx.android.synthetic.main.activity_chat_room.*
import org.commonmark.parser.Parser
import org.commonmark.renderer.html.HtmlRenderer
import org.jetbrains.anko.doAsync
import org.jsoup.Jsoup
import org.jsoup.safety.Whitelist

class ChatRoom_old : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {


  private val glide: RequestManager by lazy { Glide.with(this) }

  private val imageLoader: ImageLoader by lazy { ImageLoader(this@ChatRoom_old, glide) }

  private lateinit var room: RoomFull

  private val rawRoom: _MatrixRoom? by lazy { MatrixClient.client?.getRoom(room.room?.id) }


  private val recyclerView: EpoxyRecyclerView by lazy { findViewById<EpoxyRecyclerView>(R.id.recycler_view) }
  private val pagingController: PagingMessageController by lazy { PagingMessageController(this@ChatRoom_old) }
  private val viewModel by lazy(LazyThreadSafetyMode.NONE) {
    ViewModelProviders.of(this).get(MessageViewModel::class.java)
  }

  companion object {
    var selected: String = ""
  }

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    Sentry.getContext().recordBreadcrumb(
        BreadcrumbBuilder().setMessage("User opened a ChatRoom").build()
    )
    setContentView(R.layout.activity_chat_room)
    setSupportActionBar(toolbar)

    doAsync {
      room = getRoomByID( intent.getStringExtra("roomID"))

      /**
       * Setup DataBinding UI
       */
      val activityBinding = DataBindingUtil.setContentView<ActivityChatRoomBinding>(
              this@ChatRoom_old,
              R.layout.activity_chat_room
      )
      activityBinding.room = room

      /**
       * Init the Activity Title
       */
      title = room.dialogName

      /**
       * Setup epoxy list
       */
      recyclerView.layoutManager = LinearLayoutManager(this@ChatRoom_old)
      recyclerView.setController(pagingController)
      viewModel.messageList(room.room?.id!!).observe(this@ChatRoom_old, Observer {
//        if (SyncStarter.initialSyncDone) {
//          pagingController.submitList(it)
//        }
      })
    }



    /*doAsync {
      val intentRoomID = intent.getStringExtra("roomID")
      Logger.d(intentRoomID)
      room = getRoomByID(this@ChatRoom, intentRoomID)

      runOnUiThread {
        title = room?.dialogName
        val navigationView = findViewById<NavigationView>(R.id.nav_view)
        val headerView = navigationView.getHeaderView(0)
        headerView.findViewById<TextView>(R.id.roomTitle).text = room?.dialogName ?: "ERROR GETTING ROOM NAME"
        headerView.findViewById<TextView>(R.id.topic).text = room?.room?.topic ?: "ERROR GETTING ROOM TOPIC"
        LinkifyCompat.addLinks(headerView.findViewById<TextView>(R.id.topic), Linkify.ALL)
        val roomSidebarImage = headerView.findViewById<ImageView>(R.id.imageView)
        imageLoader.loadImage(roomSidebarImage, room?.dialogPhoto!!, room!!)

        val toggle = ActionBarDrawerToggle(
            this@ChatRoom, drawer_layout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()
        nav_view.setNavigationItemSelectedListener(this@ChatRoom)

        initMessagesListAdapter()
        initMessageInput()
      }


    }*/
  }

  /*private fun initMessageInput() {
    val input = findViewById<MessageInput>(R.id.input)
    input.setTypingListener(object : MessageInput.TypingListener {
      override fun onStartTyping() {
        // TODO link to matrix
      }

      override fun onStopTyping() {
        // TODO link to matrix
      }
    })
    input.setInputListener(this)
  }*/

  /*private fun initMessagesListAdapter() {
    messagesList = findViewById(R.id.messagesList)
    val holdersConfig = MessageHolders()
        .setIncomingTextConfig(IncomingTextMessageViewHolder::class.java, R.layout.item_custom_incoming_text_message)
        .setIncomingImageConfig(IncomingImageMessageViewHolder::class.java, R.layout.item_custom_incoming_image_message)
        .setOutcomingTextConfig(OutcomingTextMessageViewHolder::class.java, R.layout.item_custom_outcoming_text_message)
        .setOutcomingImageConfig(OutcomingImageMessageViewHolder::class.java, R.layout.item_custom_outcoming_image_message)

    messagesListAdapter = MessagesListAdapter(MatrixClient.id, holdersConfig, imageLoader)

    messagesListAdapter!!.enableSelectionMode(this)
    messagesListAdapter!!.setLoadMoreListener(this)

    messagesListAdapter!!.registerViewClickListener(R.id.messageUserAvatar) { _, message ->
      val intent = Intent(this, UserProfile::class.java)
      UserProfile.user = message?.user
      this.startActivity(intent)
    }

    messagesList?.setAdapter(messagesListAdapter)

    selected = room?.room?.id!!
    for (message in room!!.getFullMessages()) {
      runOnUiThread {
        messagesListAdapter?.addToStart(message, false)
      }
    }
  }*/

  /*override fun onContextItemSelected(item: MenuItem): Boolean {
    val contentV = findViewById<RelativeLayout>(R.id.content)
    val snackLength = Snackbar.LENGTH_SHORT
    Sentry.getContext().recordBreadcrumb(
        BreadcrumbBuilder().setMessage("User long clicked a message and triggered onContextItemSelected").build()
    )
    return when (item.itemId) {
      IncomingTextMessageViewHolder.COPY_ID -> {
        //messagesListAdapter?.copySelectedMessagesText(this, getMessageStringFormatter(), true)
        true
      }
      IncomingTextMessageViewHolder.CITE_ID -> {
        Snackbar.make(contentV, "Yet to be implemented!", snackLength).show()
        true
      }
      IncomingTextMessageViewHolder.FORWARD_ID -> {
        Snackbar.make(contentV, "Yet to be implemented!", snackLength).show()
        true
      }
      IncomingTextMessageViewHolder.PERMALINK_ID -> {
        Snackbar.make(contentV, "Yet to be implemented!", snackLength).show()
        true
      }
      IncomingTextMessageViewHolder.REPORT_ID -> {
        Snackbar.make(contentV, "Yet to be implemented!", snackLength).show()
        true
      }
      IncomingTextMessageViewHolder.SHARE_ID -> {
        Snackbar.make(contentV, "Yet to be implemented!", snackLength).show()
        true
      }
      IncomingTextMessageViewHolder.SOURCE_ID -> {
        Snackbar.make(contentV, "Yet to be implemented!", snackLength).show()
        true
      }
      OutcomingTextMessageViewHolder.REDACT_ID -> {
        Snackbar.make(contentV, "Yet to be implemented!", snackLength).show()
        true
      }
      else -> super.onContextItemSelected(item)
    }
  }*/

  override fun onBackPressed() {
    Sentry.getContext().recordBreadcrumb(
        BreadcrumbBuilder().setMessage("User closed a ChatRoom").build()
    )
    super.onBackPressed()
  }

  override fun onCreateOptionsMenu(menu: Menu): Boolean {
    // Inflate the menu; this adds items to the action bar if it is present.
    menuInflater.inflate(R.menu.chatroom_menu, menu)
    return true
  }

  override fun onOptionsItemSelected(item: MenuItem): Boolean {
    // Handle action bar item clicks here. The action bar will
    // automatically handle clicks on the Home/Up button, so long
    // as you specify a parent activity in AndroidManifest.xml.
    return when (item.itemId) {
      R.id.action_settings -> true
      else -> super.onOptionsItemSelected(item)
    }
  }

  override fun onNavigationItemSelected(item: MenuItem): Boolean {
    // Handle navigation view item clicks here.
    when (item.itemId) {

    }
    return true
  }

  private fun parseMarkdown(input: CharSequence?): String {
    Logger.d("Send Message: %s", input)
    val parser = Parser.builder()
        .build()
    val document = parser.parse(input.toString())
    val renderer = HtmlRenderer.builder()
        .softbreak("<br>")
        .build()
    val renderedMD = renderer.render(document)
    val whitelist = Whitelist.none()
    whitelist.addTags("font", "del", "h1", "h2", "h3", "h4", "h5", "h6", "blockquote", "p", "a", "ul", "ol", "sup", "sub", "li", "b", "i", "u", "strong", "em", "strike", "code", "hr", "br", "div", "table", "thead", "tbody", "tr", "th", "td", "caption", "pre", "span", "img")
    var safeRenderedMD = Jsoup.clean(renderedMD, whitelist)
    safeRenderedMD = safeRenderedMD.replace("\n", "<br>")

    return safeRenderedMD
  }

  /*override fun onSubmit(input: CharSequence?): Boolean {
    doAsync {
      val parsedText = parseMarkdown(input)

      val message = Message()
      val messageFull = MessageFull()
      messageFull.message = message

      messageFull.message?.userID = MatrixClient.id
      messageFull.message?.roomID = room?.room?.id

      val userDBL = getUsersByRoomIDFromCache(this@ChatRoom, room?.room?.id!!).filter { u -> u.MXID == message.userID }
      messageFull.usersDB = userDBL

      messageFull.message?.text = input.toString()
      messageFull.message?.textHTML = parsedText

      val randomID = Math.random().toString()
      Logger.d(randomID)
      messageFull.message?.idDB = randomID

      messageFull.message?.createdAt = Date()

      runOnUiThread {
        messagesListAdapter?.addToStart(
            messageFull,
            true
        )
      }

      doAsync {
        val id = rawRoom?.sendFormattedText(parsedText, input.toString())
        messageFull.message?.idDB = id!!
        val messages = arrayListOf(messageFull)
        saveMessagesToDB(this@ChatRoom, messages)
      }
    }

    return true
  }*/

  /*private fun getMessageStringFormatter(): MessagesListAdapter.Formatter<MessageFull> {
    return MessagesListAdapter.Formatter { message: MessageFull ->
      val createdAt = SimpleDateFormat("MMM d, EEE 'at' h:mm a", Locale.getDefault())
          .format(message.message?.createdAt)

      var text = message.message?.text
      if (text == null) text = "[attachment]"

      String.format(Locale.getDefault(), "%s: %s (%s)",
          message.getUser().name, text, createdAt)
    }
  }*/
}
