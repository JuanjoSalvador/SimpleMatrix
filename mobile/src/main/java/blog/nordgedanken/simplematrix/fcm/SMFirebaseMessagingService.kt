package blog.nordgedanken.simplematrix.fcm

import blog.nordgedanken.matrix_android_sdk.account.Account
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.orhanobut.logger.Logger
import io.kamax.matrix.json.GsonUtil
import org.jetbrains.anko.doAsync
import org.koin.android.ext.android.inject
import org.koin.standalone.KoinComponent


/**
 * Created by MTRNord on 23.12.2018.
 */
class SMFirebaseMessagingService : FirebaseMessagingService(), KoinComponent {
    val account: Account by inject()

    /**
     * Called if InstanceID token is updated. This may occur if the security of
     * the previous token had been compromised. Note that this is called when the InstanceID token
     * is initially generated so this is where you would retrieve the token.
     */
    override fun onNewToken(token: String?) {
        Logger.d("Refreshed token: $token")

        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // Instance ID token to your app server.
        sendRegistrationToServer(token)
    }

    /**
     * Persist token to third-party servers.
     *
     * Modify this method to associate the user's FCM InstanceID token with any server-side account
     * maintained by your application.
     *
     * @param token The new token.
     */
    private fun sendRegistrationToServer(token: String?) {
        val data = SetPusher()
        data.lang = "en"
        data.kind = "http"
        data.appDisplayName = "SimpleMatrix"
        data.deviceDisplayName = android.os.Build.MODEL
        data.appId = "blog.nordgedanken.simplematrix"
        data.pushkey = token
        data.data = Data()
        data.data.url = "https://push.nordgedanken.de/_matrix/push/v1/notify"
        data.append = false
        val json = GsonUtil.makeObj(data)

        doAsync {
            account.client?.setPusher(json)
        }
    }

    override fun onMessageReceived(remoteMessage: RemoteMessage?) {
        // ...

        // TODO(developer): Handle FCM messages here.
        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        Logger.d("From: ${remoteMessage?.from}")

        // Check if message contains a data payload.
        remoteMessage?.data?.isNotEmpty()?.let {
            Logger.d("Message data payload: " + remoteMessage.data)

        }

        // Check if message contains a notification payload.
        remoteMessage?.notification?.let {
            Logger.d("Message Notification Body: ${it.body}")
        }

        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.
    }

}