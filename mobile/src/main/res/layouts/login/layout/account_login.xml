<?xml version="1.0" encoding="utf-8"?><!--
  ~ SimpleMatrix - A simplified Android Matrix Client
  ~ Copyright (C) 2018 Marcel Radzio
  ~
  ~ This program is free software: you can redistribute it and/or modify
  ~ it under the terms of the GNU Affero General Public License as
  ~ published by the Free Software Foundation, either version 3 of the
  ~ License, or (at your option) any later version.
  ~
  ~  This program is distributed in the hope that it will be useful,
  ~  but WITHOUT ANY WARRANTY; without even the implied warranty of
  ~  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  ~  GNU Affero General Public License for more details.
  ~
  ~  You should have received a copy of the GNU Affero General Public License
  ~  along with this program. If not, see <http://www.gnu.org/licenses/>.
  -->

<RelativeLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:gravity="center_horizontal"
    android:orientation="vertical"
    android:paddingLeft="@dimen/activity_horizontal_margin"
    android:paddingTop="@dimen/activity_vertical_margin"
    android:paddingRight="@dimen/activity_horizontal_margin"
    android:paddingBottom="@dimen/activity_vertical_margin"
    tools:context=".MatrixAccountActivity">

    <!-- Login progress -->
    <blog.nordgedanken.simplematrix.utils.ui.ProgressBar
        android:id="@+id/login_progress"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:layout_centerInParent="true"
        android:indeterminate="true"
        android:visibility="gone" />

    <androidx.constraintlayout.widget.ConstraintLayout
        android:id="@+id/login_form"
        android:layout_width="match_parent"
        android:layout_height="match_parent">

        <ImageView
            android:id="@+id/imageView"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:layout_marginStart="8dp"
            android:layout_marginTop="8dp"
            android:layout_marginEnd="8dp"
            android:contentDescription="@string/simplematrix_logo_description"
            android:src="@mipmap/ic_launcher"
            app:layout_constraintEnd_toEndOf="parent"
            app:layout_constraintStart_toStartOf="parent"
            app:layout_constraintTop_toTopOf="parent" />

        <androidx.constraintlayout.widget.ConstraintLayout
            android:id="@+id/constraintLayout"
            android:layout_width="0dp"
            android:layout_height="wrap_content"
            android:layout_marginStart="8dp"
            android:layout_marginTop="8dp"
            android:layout_marginEnd="8dp"
            app:layout_constraintEnd_toEndOf="parent"
            app:layout_constraintStart_toStartOf="parent"
            app:layout_constraintTop_toBottomOf="@+id/imageView">

            <TextView
                android:id="@+id/AT"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:layout_marginEnd="8dp"
                android:gravity="center"
                android:text="\@"
                android:textAllCaps="false"
                android:textSize="24sp"
                android:textStyle="bold"
                app:layout_constraintBottom_toBottomOf="parent"
                app:layout_constraintEnd_toStartOf="@+id/localpartInputLayout"
                app:layout_constraintStart_toStartOf="parent"
                app:layout_constraintTop_toTopOf="parent"
                tools:ignore="HardcodedText" />

            <com.google.android.material.textfield.TextInputLayout
                android:id="@+id/localpartInputLayout"
                style="@style/Widget.MaterialComponents.TextInputLayout.OutlinedBox"
                android:layout_width="0dp"
                android:layout_height="wrap_content"
                android:layout_marginStart="8dp"
                android:hint="@string/prompt_matrix_id"
                app:errorEnabled="true"
                app:layout_constraintBottom_toBottomOf="parent"
                app:layout_constraintEnd_toStartOf="@+id/colon"
                app:layout_constraintStart_toEndOf="@+id/AT"
                app:layout_constraintTop_toTopOf="parent">

                <com.google.android.material.textfield.TextInputEditText
                    android:id="@+id/localpart"
                    android:layout_width="match_parent"
                    android:layout_height="wrap_content"
                    android:autofillHints="username"
                    android:importantForAutofill="yes"
                    android:inputType="textPersonName"
                    android:maxLines="1"
                    android:nextFocusDown="@id/homeserverurl"
                    android:singleLine="true" />

            </com.google.android.material.textfield.TextInputLayout>

            <TextView
                android:id="@+id/colon"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:layout_marginStart="8dp"
                android:layout_marginEnd="8dp"
                android:gravity="center"
                android:text=":"
                android:textAllCaps="false"
                android:textSize="24sp"
                android:textStyle="bold"
                app:layout_constraintBottom_toBottomOf="parent"
                app:layout_constraintEnd_toStartOf="@+id/homeserverurlInputLayout"
                app:layout_constraintStart_toEndOf="@+id/localpartInputLayout"
                app:layout_constraintTop_toTopOf="parent"
                tools:ignore="HardcodedText" />

            <com.google.android.material.textfield.TextInputLayout
                android:id="@+id/homeserverurlInputLayout"
                style="@style/Widget.MaterialComponents.TextInputLayout.OutlinedBox"
                android:layout_width="0dp"
                android:layout_height="wrap_content"
                android:hint="@string/prompt_homeserverurl"
                app:errorEnabled="true"
                app:layout_constraintBottom_toBottomOf="parent"
                app:layout_constraintEnd_toEndOf="parent"
                app:layout_constraintStart_toEndOf="@+id/colon"
                app:layout_constraintTop_toTopOf="parent">

                <com.google.android.material.textfield.TextInputEditText
                    android:id="@+id/homeserverurl"
                    android:layout_width="match_parent"
                    android:layout_height="wrap_content"
                    android:importantForAutofill="yes"
                    android:inputType="textUri"
                    android:maxLines="1"
                    android:nextFocusDown="@id/password"
                    android:singleLine="true" />
            </com.google.android.material.textfield.TextInputLayout>
        </androidx.constraintlayout.widget.ConstraintLayout>

        <com.google.android.material.textfield.TextInputLayout
            android:id="@+id/passwordInputLayout"
            style="@style/Widget.MaterialComponents.TextInputLayout.OutlinedBox"
            android:layout_width="0dp"
            android:layout_height="wrap_content"
            android:layout_marginStart="8dp"
            android:layout_marginEnd="8dp"
            android:hint="@string/prompt_password"
            app:errorEnabled="true"
            app:layout_constraintEnd_toEndOf="parent"
            app:layout_constraintStart_toStartOf="parent"
            app:layout_constraintTop_toBottomOf="@+id/constraintLayout">

            <com.google.android.material.textfield.TextInputEditText
                android:id="@+id/password"
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                android:autofillHints="password"
                android:imeActionId="6"
                android:imeActionLabel="@string/action_sign_in"
                android:imeOptions="actionUnspecified"
                android:importantForAutofill="yes"
                android:inputType="textPassword"
                android:maxLines="1"
                android:singleLine="true" />

        </com.google.android.material.textfield.TextInputLayout>

        <com.google.android.material.textfield.TextInputLayout
            android:id="@+id/serverInputLayout"
            style="@style/Widget.MaterialComponents.TextInputLayout.OutlinedBox"
            android:layout_width="0dp"
            android:layout_height="wrap_content"
            android:layout_marginStart="8dp"
            android:layout_marginTop="8dp"
            android:layout_marginEnd="8dp"
            android:hint="@string/prompt_server"
            app:errorEnabled="true"
            app:layout_constraintEnd_toEndOf="parent"
            app:layout_constraintStart_toStartOf="parent"
            app:layout_constraintTop_toBottomOf="@+id/passwordInputLayout">

            <com.google.android.material.textfield.TextInputEditText
                android:id="@+id/server"
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                android:inputType="textUri"
                android:maxLines="1"
                android:nextFocusDown="@id/sign_in_button"
                android:singleLine="true" />

        </com.google.android.material.textfield.TextInputLayout>

        <com.google.android.material.button.MaterialButton
            android:id="@+id/sign_in_button"
            style="@style/Widget.MaterialComponents.Button"
            android:layout_width="0dp"
            android:layout_height="wrap_content"
            android:layout_marginStart="8dp"
            android:layout_marginEnd="8dp"
            android:layout_marginBottom="8dp"
            android:onClick="login"
            android:text="@string/action_sign_in"
            android:textAppearance="@android:style/TextAppearance.Material.Medium"
            app:background="@color/primaryDarkColor"
            app:layout_constraintEnd_toEndOf="parent"
            app:layout_constraintStart_toStartOf="parent"
            app:layout_constraintTop_toBottomOf="@id/serverInputLayout"
            app:layout_constraintVertical_bias="0.0"
            tools:ignore="NewApi" />

    </androidx.constraintlayout.widget.ConstraintLayout>
</RelativeLayout>