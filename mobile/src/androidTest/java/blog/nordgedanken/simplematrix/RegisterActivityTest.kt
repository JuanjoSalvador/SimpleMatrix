/*
 * SimpleMatrix - A simplified Android Matrix Client
 * Copyright (C) 2018 Marcel Radzio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package blog.nordgedanken.simplematrix

/**
 * Created by MTRNord on 15.08.2018.
 */
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.closeSoftKeyboard
import androidx.test.espresso.action.ViewActions.typeText
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import androidx.test.rule.ActivityTestRule
import org.hamcrest.core.IsNull
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
@LargeTest
class RegisterActivityTest {
    private var mActivity: MatrixCreateAccountActivity? = null

    @get:Rule
    var mActivityRule: ActivityTestRule<MatrixCreateAccountActivity> = ActivityTestRule(MatrixCreateAccountActivity::class.java)

    @Before
    fun setUp() {
        mActivity = mActivityRule.activity
        ViewMatchers.assertThat(mActivity, IsNull.notNullValue())
    }

    @Test
    fun matrixIDInputShouldWork() {
        val username = "MTRNord"

        // Type text
        onView(withId(R.id.matrix_id))
                .perform(typeText(username), closeSoftKeyboard())

        // Check that the text was changed.
        onView(withId(R.id.matrix_id))
                .check(matches(withText(username)))
    }

    @Test
    fun passwordInputShouldWork() {
        val password = "Test1234"
        // Type text
        onView(withId(R.id.password))
                .perform(typeText(password), closeSoftKeyboard())

        // Check that the text was changed.
        onView(withId(R.id.password))
                .check(matches(withText(password)))
    }

    @Test
    fun serverInputShouldWork() {
        val server = "matrix.ffslfl.net"
        // Type text
        onView(withId(R.id.server))
                .perform(typeText(server), closeSoftKeyboard())

        // Check that the text was changed.
        onView(withId(R.id.server))
                .check(matches(withText(server)))
    }
}