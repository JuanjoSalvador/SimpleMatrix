/*
 * SimpleMatrix - A simplified Android Matrix Client
 * Copyright (C) 2018 Marcel Radzio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package blog.nordgedanken.simplematrix

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import androidx.test.rule.ActivityTestRule
import blog.nordgedanken.simplematrix.roomView.RoomsActivity
import blog.nordgedanken.simplematrix.roomView.tabFragments.ChatsFragment
import blog.nordgedanken.simplematrix.roomView.tabFragments.DirectChatsFragment
import blog.nordgedanken.simplematrix.roomView.tabFragments.FavsFragment
import org.hamcrest.Matchers.allOf
import org.hamcrest.Matchers.instanceOf
import org.hamcrest.core.IsNull.notNullValue
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.Timeout
import org.junit.runner.RunWith
import java.util.concurrent.TimeUnit

/**
 * Created by MTRNord on 15.08.2018.
 */
@RunWith(AndroidJUnit4::class)
@LargeTest
class RoomsActivityTest {

    private var mActivity: RoomsActivity? = null

    @get:Rule
    var mActivityRule: ActivityTestRule<RoomsActivity> = ActivityTestRule(RoomsActivity::class.java)

    @get:Rule
    var globalTimeout: Timeout = Timeout.seconds(200) // 200 seconds max per method tested

    @Before
    fun setUp() {
        mActivity = mActivityRule.activity
        assertThat(mActivity, notNullValue())
    }

    @Test
    fun swipePage() {
        onView(withId(R.id.viewpager))
                .check(matches(isDisplayed()))

        onView(withId(R.id.viewpager))
                .perform(swipeLeft())

        onView(withId(R.id.viewpager))
                .perform(swipeRight())
    }

    @Test
    fun checkTabLayoutDisplayed() {
        onView(withId(R.id.tablayout))
                .perform(click())
                .check(matches(isDisplayed()))
    }

    @Test
    fun checkFavsIsWorking() {
        onView(withId(R.id.tablayout))
                .perform(click())
                .check(matches(isDisplayed()))

        TimeUnit.SECONDS.sleep(30)

        onView(allOf(withContentDescription(mActivity?.resources?.getText(R.string.tab_favorites).toString()), isDescendantOfA(withId(R.id.tablayout))))
                .perform(click())
                .check(matches(isDisplayed()))

        val currentItem = mActivity?.viewPager?.currentItem
        val tag = "android:switcher:" + R.id.viewpager + ":" + currentItem
        val favsFragment = mActivity?.supportFragmentManager?.findFragmentByTag(tag)
        assertThat(favsFragment, instanceOf(FavsFragment::class.java))
    }

    @Test
    fun checkRoomsIsWorking() {
        onView(withId(R.id.tablayout))
                .perform(click())
                .check(matches(isDisplayed()))

        onView(allOf(withContentDescription(mActivity?.resources?.getText(R.string.tab_chats).toString()), isDescendantOfA(withId(R.id.tablayout))))
                .perform(click())
                .check(matches(isDisplayed()))

        val currentItem = mActivity?.viewPager?.currentItem
        val tag = "android:switcher:" + R.id.viewpager + ":" + currentItem
        val chatFragment = mActivity?.supportFragmentManager?.findFragmentByTag(tag)
        assertThat(chatFragment, instanceOf(ChatsFragment::class.java))
    }

    @Test
    fun checkPeopleIsWorking() {
        onView(withId(R.id.tablayout))
                .perform(click())
                .check(matches(isDisplayed()))

        onView(allOf(withContentDescription(mActivity?.resources?.getText(R.string.tab_direct_chats).toString()), isDescendantOfA(withId(R.id.tablayout))))
                .perform(click())
                .check(matches(isDisplayed()))

        val currentItem = mActivity?.viewPager?.currentItem
        val tag = "android:switcher:" + R.id.viewpager + ":" + currentItem
        val directChatsFragment = mActivity?.supportFragmentManager?.findFragmentByTag(tag)
        assertThat(directChatsFragment, instanceOf(DirectChatsFragment::class.java))
    }

    @Test
    fun checkTabSwitch() {
        onView(withId(R.id.tablayout)).check(matches(isDisplayed()))

        TimeUnit.SECONDS.sleep(30)

        onView(allOf(withContentDescription(mActivity?.resources?.getText(R.string.tab_favorites).toString()), isDescendantOfA(withId(R.id.tablayout))))
                .perform(click())
                .check(matches(isDisplayed()))

        val favsFragment = mActivity?.supportFragmentManager?.findFragmentByTag("android:switcher:" + R.id.viewpager + ":" + mActivity?.viewPager?.currentItem)
        assertThat(favsFragment, instanceOf(FavsFragment::class.java))

        onView(allOf(withContentDescription(mActivity?.resources?.getText(R.string.tab_chats).toString()), isDescendantOfA(withId(R.id.tablayout))))
                .perform(click())
                .check(matches(isDisplayed()))

        val chatFragment = mActivity?.supportFragmentManager?.findFragmentByTag("android:switcher:" + R.id.viewpager + ":" + mActivity?.viewPager?.currentItem)
        assertThat(chatFragment, instanceOf(ChatsFragment::class.java))

        onView(allOf(withContentDescription(mActivity?.resources?.getText(R.string.tab_direct_chats).toString()), isDescendantOfA(withId(R.id.tablayout))))
                .perform(click())
                .check(matches(isDisplayed()))

        val directChatsFragment = mActivity?.supportFragmentManager?.findFragmentByTag("android:switcher:" + R.id.viewpager + ":" + mActivity?.viewPager?.currentItem)
        assertThat(directChatsFragment, instanceOf(DirectChatsFragment::class.java))
    }
}
