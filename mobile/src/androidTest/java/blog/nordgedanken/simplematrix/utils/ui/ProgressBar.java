package blog.nordgedanken.simplematrix.utils.ui;

import android.content.Context;
import android.util.AttributeSet;
import android.view.animation.Animation;

/**
 * Progressbar that is used in UI Tests
 * Prevents the progressbar from ever showing and animating
 * Thus allowing Espresso to continue with tests and Espresso won't be blocked
 */
public class ProgressBar extends android.widget.ProgressBar {

    public ProgressBar(Context context) {
        super(context);
        setUpView();
    }

    public ProgressBar(Context context, AttributeSet attrs) {
        super(context, attrs);
        setUpView();
    }

    public ProgressBar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setUpView();
    }

    private void setUpView() {
        this.setVisibility(GONE);
    }

    @Override
    public void setVisibility(int v) {
        // Progressbar should never show
        v = GONE;
        super.setVisibility(v);
    }

    @Override
    public void startAnimation(Animation animation) {
        // Do nothing in test cases, to not block ui thread
    }
}