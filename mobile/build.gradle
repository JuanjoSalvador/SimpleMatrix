/*
 * SimpleMatrix - A simplified Android Matrix Client
 * Copyright (C) 2018 Marcel Radzio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

apply plugin: 'com.android.application'
apply plugin: 'kotlin-android'
apply plugin: 'kotlin-android-extensions'
apply plugin: 'kotlin-kapt'
apply plugin: "jacoco"

apply plugin: 'io.sentry.android.gradle'
apply plugin: 'org.jetbrains.kotlin.android.extensions'
apply plugin: 'org.jetbrains.dokka-android'

androidExtensions {
    experimental = true
}

dokka {
    outputFormat = 'javadoc'
    outputDirectory = "${project.rootDir}/javadoc"
    skipEmptyPackages = true // Do not create index pages for empty packages
}

kapt {
    correctErrorTypes = true
}

android {
    compileSdkVersion 28
    defaultConfig {
        applicationId "blog.nordgedanken.simplematrix"
        minSdkVersion 19
        targetSdkVersion 28
        versionName "0.1.0-SNAPSHOT-$gitVersionCode"
        versionCode gitVersionCode
        testInstrumentationRunner "androidx.test.runner.AndroidJUnitRunner"
        vectorDrawables.useSupportLibrary = true
        multiDexEnabled true
        javaCompileOptions {
            annotationProcessorOptions {
                arguments = ["room.schemaLocation":
                                     "$projectDir/schemas".toString()]
            }
        }
    }
    compileOptions {
        sourceCompatibility 1.8
        targetCompatibility 1.8
    }
    applicationVariants.all { variant ->
        variant.resValue "string", "versionName", "Version: $variant.versionName"
    }
    buildTypes {
        release {
            minifyEnabled true
            proguardFiles getDefaultProguardFile('proguard-android.txt'), 'proguard-rules.pro'
        }
        debug {
            //minifyEnabled true
            //proguardFiles getDefaultProguardFile('proguard-android.txt'), 'proguard-rules.pro'
            //testCoverageEnabled true
        }
    }
    configurations {
        all {
            exclude module: 'commons-logging'
        }
    }
    packagingOptions {
        exclude 'META-INF/DEPENDENCIES'
        exclude 'META-INF/LICENSE'
        exclude 'META-INF/LICENSE.txt'
        exclude 'META-INF/license.txt'
        exclude 'META-INF/NOTICE'
        exclude 'META-INF/NOTICE.txt'
        exclude 'META-INF/notice.txt'
        exclude 'META-INF/ASL2.0'
        exclude 'androidsupportmultidexversion.txt'
    }
    sourceSets {
        main {
            res.srcDirs =
                    [
                            'src/main/res/layouts/login',
                            'src/main/res/layouts/chatRoom',
                            'src/main/res/layouts/chatRoom/userProfiles',
                            'src/main/res/layouts/chatRoom/roomDetails',
                            'src/main/res/layouts',
                            'src/main/res'
                    ]
            java {
                srcDir "${buildDir.absolutePath}/generated/source/kaptKotlin/"
            }
        }
    }
    testBuildType "debug"
    testOptions {
        unitTests {
            includeAndroidResources = true
            unitTests.returnDefaultValues = true
        }
    }
    buildToolsVersion '28.0.3'
    dataBinding {
        enabled = true
    }
    useLibrary 'android.test.runner'

    useLibrary 'android.test.base'
    useLibrary 'android.test.mock'
}

project.android.buildTypes.all { buildType ->
    buildType.javaCompileOptions.annotationProcessorOptions.arguments =
            [
                    // Validation is disabled in production to remove the runtime overhead
                    validateEpoxyModelUsage: String.valueOf(buildType.name == 'debug'),
            ]
}

dependencies {
    implementation project(':matrix_android_sdk')
    implementation fileTree(include: ['*.jar'], dir: 'libs')

    // Kotlin
    implementation "org.jetbrains.kotlin:kotlin-stdlib-jdk8:1.3.21"
    implementation "org.jetbrains.anko:anko-commons:0.10.6" // Helpers
    implementation "org.jetbrains.kotlinx:kotlinx-coroutines-core:$COROUTINES_VERSION"
    implementation "org.jetbrains.kotlinx:kotlinx-coroutines-android:$COROUTINES_VERSION"

    // AndroidX
    implementation 'androidx.appcompat:appcompat:1.0.2'
    implementation 'androidx.core:core-ktx:1.1.0-alpha04'
    kapt 'androidx.annotation:annotation:1.0.1' // AndroidX annotations
    implementation 'androidx.constraintlayout:constraintlayout:2.0.0-alpha3'
    implementation 'androidx.preference:preference:1.0.0'
    androidTestImplementation 'androidx.test:rules:1.1.1'
    androidTestImplementation "androidx.test.ext:junit:1.1.0"
    androidTestImplementation 'androidx.test:rules:1.1.1'

    // Android Core
    implementation 'com.android.support:multidex:1.0.3'

    // Android Architecture
    implementation "androidx.paging:paging-runtime-ktx:$PAGING_VERSION" // use -ktx for Kotlin
    implementation "androidx.room:room-runtime:$ROOM_VERSION"
    kapt "androidx.room:room-compiler:$ROOM_VERSION"
    testImplementation "androidx.room:room-testing:$ROOM_VERSION"
    implementation "androidx.lifecycle:lifecycle-extensions:$LIFECYCLE_VERSION"
    kapt "androidx.lifecycle:lifecycle-compiler:$LIFECYCLE_VERSION"

    // Android Tests
    testImplementation 'junit:junit:4.12'
    androidTestImplementation 'androidx.test.espresso:espresso-core:3.1.1'
    testImplementation 'org.mockito:mockito-core:2.23.0'
    testImplementation 'androidx.test:core:1.1.0'
    androidTestImplementation 'com.squareup.okhttp3:mockwebserver:3.11.0'

    // Android Material
    implementation 'com.google.android.material:material:1.1.0-alpha03'

    // Java 8 backports
    implementation 'net.sourceforge.streamsupport:streamsupport:1.7.0'

    // HTTP Libs
    implementation 'com.squareup.okhttp3:okhttp:3.13.1'
    implementation 'com.squareup.okhttp3:okhttp-tls:3.13.1'
    implementation "com.facebook.stetho:stetho:$STETHO_VERSION" // Network debugging
    implementation "com.facebook.stetho:stetho-okhttp3:$STETHO_VERSION" // Network debugging okhttp3 helper

    // Matrix SDK by Maximus
    implementation "io.kamax:matrix-java-sdk:$MATRIX_JAVA_SDK_VERSION" // Matrix SDK

    // Epoxy is an Android library for building complex screens in a RecyclerView.
    implementation "com.airbnb.android:epoxy:$EPOXY_VERSION"
    implementation "com.airbnb.android:epoxy-databinding:$EPOXY_VERSION"
    implementation "com.airbnb.android:epoxy-paging:$EPOXY_VERSION"
    kapt "com.airbnb.android:epoxy-processor:$EPOXY_VERSION"

    // Images
    implementation "com.github.bumptech.glide:glide:$GLIDE_VERSION" // Image loading
    implementation "com.github.bumptech.glide:okhttp3-integration:$GLIDE_VERSION"
    kapt "com.github.bumptech.glide:compiler:$GLIDE_VERSION" // Image Loading Annotations
    implementation 'de.hdodenhof:circleimageview:2.2.0' // Round Images
    implementation 'com.github.siyamed:android-shape-imageview:0.9.3@aar' // Shaped images

    // Logging
    implementation 'com.orhanobut:logger:2.2.0' // Logger
    implementation 'org.slf4j:slf4j-api:1.7.25' // Logging of matrix-sdk
    implementation 'com.github.tony19:logback-android:1.1.1-12' // Logging of matrix-sdk

    // Message Processing
    implementation 'com.atlassian.commonmark:commonmark:0.11.0' // Commonmark parser
    implementation 'org.jsoup:jsoup:1.11.3' // HTML parser to remove not allowed tags from the formatted String when sending

    // Json Lib
    implementation 'com.google.code.gson:gson:2.8.5' // Json Lib

    // Animations
    implementation 'com.github.ybq:Android-SpinKit:1.2.0'

    // DeepLink Helpers
    implementation "com.github.airbnb.DeepLinkDispatch:deeplinkdispatch:eb7e520d87"
    kapt "com.github.airbnb.DeepLinkDispatch:deeplinkdispatch-processor:eb7e520d87"

    // Crash Reporting (Sentry)
    implementation 'io.sentry:sentry-android:1.7.10'

    // Firebase
    implementation 'com.google.firebase:firebase-core:16.0.7'
    implementation 'com.google.firebase:firebase-messaging:17.3.4'

    // Dependency injection
    implementation "org.koin:koin-core:$KOIN_VERSION"
    implementation "org.koin:koin-android:$KOIN_VERSION"
    implementation "org.koin:koin-androidx-viewmodel:$KOIN_VERSION"
}

apply plugin: 'com.google.gms.google-services'
