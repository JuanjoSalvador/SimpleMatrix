# SimpleMatrix

[![](https://img.shields.io/matrix/SimpleMatrix:matrix.ffslfl.net.svg?label=Matrix&style=flat)](https://matrix.to/#/#SimpleMatrix:matrix.ffslfl.net)  ![](https://img.shields.io/f-droid/v/blog.nordgedanken.simplematrix.svg?style=flat)

![Preview](images/stitched.jpg)

## Idea behind this
The Idea is to make a Matrix Client which is simpler in terms of UI and UX (design and usage).

The reason I want to do this instead of improving the Riot one is simple:
1. The Riot Client will from what I heard get at some point a new design anyway
2. I want to go a different approach than Riot does and want to keep the design more Whatsapp or Telegram like
3. I am trying to get this Client a lot techy like and so want to start a fresh App from the ground up.

So the goal is to make a simple App that at some point gets full featured with a different design than Riot has currently.

## How do I get this?
Currently there is a Nightly repo for F-Droid available. You can either add
`https://gitlab.com/Nordgedanken/simplematrix/SimpleMatrix-nightly/raw/master/fdroid/repo` as Source in your
F-Droid App or use the QR Code over at https://gitlab.com/Nordgedanken/simplematrix/SimpleMatrix-nightly .
This is also where this nightly repository gets auto deployed too.

*Google Play is currently impossible for me as I don't own a Credit Card!*

## Supported Android Versions

Currently I do support Android down to version 4.4 but this comes with some Problems.
While I have support for them they might have some bugs because I do test using a newer Android versions. I will try to fix these issues As soon as I see them in Sentry.

## Dependencies

- [Sentry.io](https://sentry.io) is being used to be able to get automatic Bug reports. All of them will NOT track any user
specific data as far as possible. (Android version and Phone type for example currently get tracked)
- [Kamax Matrix Client SDK for Java](https://github.com/kamax-io/matrix-java-sdk) is being used as the connector to the Matrix
- [Epoxy](https://github.com/airbnb/epoxy) is being used for the Room List, timeline and other scroll stuff.
- [Glide](https://github.com/bumptech/glide) is being used for Image loading
- [Commonmark java parser by Atlassin](https://github.com/atlassian/commonmark-java) is being used to parse Commonmark while sending messages
- [Jsoup](https://jsoup.org/) is being used to only send allowed HTML tags in messages
- [Gson](https://github.com/google/gson) is being used to parse JSON
- [Stetho](http://facebook.github.io/stetho/) is being used for easier network debugging

## Why Sentry?

As you might have noticed above I do use Sentry.io for automatic bug reports.
This is currently done as I plan to have rapid development which means I want to know Bugs early.
I know some of you might have problems with using third party services but it helps me as a Developer a lot more than any alternatives.

Information I can see is fully anonymized.
But I do see information about your phone and system which help me to pin down if a error only is one one specific type of phone or happens to all users.

## Contribute
Contributions and PRs are welcome to turn this into a fully fledged Matrix Client.
Your code will be licensed under AGPLv3.

## Contact
Matrix:
- Join #SimpleMatrix:matrix.ffslfl.net using https://matrix.to/#/#SimpleMatrix:matrix.ffslfl.net (Markdown parser doesn't like Matrix.to links somehow)
