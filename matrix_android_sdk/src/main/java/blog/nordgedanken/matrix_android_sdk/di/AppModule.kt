package blog.nordgedanken.matrix_android_sdk.di

import android.content.Context
import blog.nordgedanken.matrix_android_sdk.account.Account
import blog.nordgedanken.matrix_android_sdk.api.Api
import blog.nordgedanken.matrix_android_sdk.api.viewModels.RoomList
import com.facebook.stetho.okhttp3.StethoInterceptor
import okhttp3.OkHttpClient
import org.koin.android.ext.koin.androidApplication
import org.koin.androidx.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.module
import retrofit2.Retrofit
import java.util.concurrent.TimeUnit

/**
 * Created by MTRNord on 09.02.2019.
 */
class SDKAppModule(private val context: Context) {
    val definition = module {
        single<Api.MatrixService> {
            val account: Account = get()
            val retrofit = Retrofit.Builder()
                    .baseUrl(account.client?.context?.domain!!)
                    .build()

            retrofit.create(Api.MatrixService::class.java)
        }

        single { Account() }

        viewModel {
            RoomList(this.androidApplication(), get())
        }

        single {
            OkHttpClient.Builder()
                    .addNetworkInterceptor(StethoInterceptor())
                    .connectTimeout(30 * 1000, TimeUnit.MILLISECONDS)
                    .writeTimeout(5, TimeUnit.MINUTES)
                    .readTimeout(5, TimeUnit.MINUTES).followRedirects(false)
                    .build()
        }
    }
}