package blog.nordgedanken.matrix_android_sdk.api.json

data class ChunkItem(
	val roomId: String? = null,
	val aliases: List<String?>? = null,
	val worldReadable: Boolean? = null,
	val topic: String? = null,
	val canonicalAlias: String? = null,
	val numJoinedMembers: Int? = null,
	val guestCanJoin: Boolean? = null,
	val name: String? = null,
	val avatarUrl: String? = null,
	val isPublic: Boolean? = null
)
