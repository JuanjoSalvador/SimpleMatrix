package blog.nordgedanken.matrix_android_sdk.api.json

data class PublicRooms(
	val totalRoomCountEstimate: Int? = null,
	val nextBatch: String? = null,
	val chunk: List<ChunkItem?>? = null
)
