package blog.nordgedanken.matrix_android_sdk

import blog.nordgedanken.matrix_android_sdk.account.Account
import blog.nordgedanken.matrix_android_sdk.authentication.LoginResult
import com.orhanobut.logger.Logger
import io.kamax.matrix._MatrixID
import io.kamax.matrix.client.MatrixClientContext
import io.kamax.matrix.client.MatrixClientRequestException
import io.kamax.matrix.client.MatrixPasswordCredentials
import io.kamax.matrix.client.regular.MatrixHttpClient
import okhttp3.OkHttpClient
import org.koin.standalone.KoinComponent
import org.koin.standalone.inject
import java.net.MalformedURLException
import java.net.URL
import java.net.UnknownHostException

/**
 * The entrypoint class of the SDK
 * Created by MTRNord on 09.02.2019.
 */
class Matrix : KoinComponent {
    enum class State {
        LoggedIn, LoggedOut, Error, Uninitialized, InitialSync, BackgroundSync
    }

    private val okHttpClient: OkHttpClient by inject()
    val account: Account by inject()
    var status: State = State.Uninitialized
    var errorReason: LoginResult? = null

    /**
     * Normal Login
     */
    constructor(mxid: _MatrixID, server: String, password: String) {
        val context = MatrixClientContext()
        val url = try {
            URL(server)
        } catch (e: MalformedURLException) {
            URL("https://$server")
        }
        context.hsBaseUrl = url
        context.initialDeviceName = "SimpleMatrix"
        account.client = MatrixHttpClient(context, okHttpClient)

        val credentials = MatrixPasswordCredentials(mxid.localPart, password)

        try {
            account.client?.context?.initialDeviceName = "SimpleMatrix"
            account.client?.login(credentials)
        } catch (e: MatrixClientRequestException) {
            if (e.cause is UnknownHostException) {
                status = State.Error
                errorReason = LoginResult.ServerNotFound
            }
            Logger.d(e)
            Logger.d(e.error)
            val error = e.error
            if (error.isPresent) {
                Logger.e(error.get().errcode + " :" + error.get().error)
                if (error.get().errcode == "M_FORBIDDEN" && error.get().error == "Invalid password") {
                    errorReason = LoginResult.WrongPasswordOrUserNotFound
                    status = State.Error
                }
            }

        } catch (e: IllegalStateException) {
            Logger.e("Login unsuccessful", e)
            status = State.Error
            errorReason = LoginResult.UnknownError
        }

        status = State.LoggedIn
    }

    /**
     * Login well-known
     */
    constructor(mxid: _MatrixID, password: String) {
        val context = MatrixClientContext()
        context.initialDeviceName = "SimpleMatrix"
        context.domain = mxid.domain
        account.client = MatrixHttpClient(context, okHttpClient)

        try {
            account.client?.discoverSettings()
        } catch (e: IllegalStateException) {
            if (e.message == "No valid Homeserver base URL was found") {
                status = State.Error
                errorReason = LoginResult.WellKnownURLNotFound
            }
        }

        val credentials = MatrixPasswordCredentials(mxid.localPart, password)

        try {
            account.client?.login(credentials)
        } catch (e: MatrixClientRequestException) {
            if (e.cause is UnknownHostException) {
                status = State.Error
                errorReason = LoginResult.ServerNotFound
            }
            Logger.d(e)
            Logger.d(e.error)
            val error = e.error
            if (error.isPresent) {
                Logger.e(error.get().errcode + " :" + error.get().error)
                if (error.get().errcode == "M_FORBIDDEN" && error.get().error == "Invalid password") {
                    status = State.Error
                    errorReason = LoginResult.WrongPasswordOrUserNotFound
                }
            }
        } catch (e: IllegalStateException) {
            Logger.e("Login unsuccessful", e)
            status = State.Error
            errorReason = LoginResult.UnknownError
        }

        status = State.LoggedIn
    }

    /**
     * Relogin
     */
    constructor(mxid: _MatrixID, baseUrl: String, domain: String, token: String) {
        val context = MatrixClientContext()
        context.initialDeviceName = "SimpleMatrix"
        val url = try {
            URL(baseUrl)
        } catch (e: MalformedURLException) {
            URL("https://$baseUrl")
        }
        context.hsBaseUrl = url
        context.domain = domain
        context.setUser(mxid)
        context.token = token
        account.client = MatrixHttpClient(context, okHttpClient)
        status = State.LoggedIn
    }
}